﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor.Experimental.AssetImporters;
using UnityEngine;


[ScriptedImporter(1,"svg")]
public class SvgEditor : ScriptedImporter
{

    public override void OnImportAsset(AssetImportContext ctx)
    {
        var cube = new SVGPainting();
        var text = File.ReadAllText(ctx.assetPath);
        cube.File = text;
        //Bitmap bmp = svgDocument.Draw(12, 12);

        //renderer.sprite 
        ctx.AddObjectToAsset("main obj", cube);
        ctx.SetMainObject(cube);



    }


    
}
