﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[Serializable]
public class EventProcedure : UnityEvent { }
public class EventPFunction<T> : UnityEvent<T> { }
public class CallEvents : MonoBehaviour
{
    [SerializeField] public EventProcedure _eventProcedure;
    

    public void CallAllFunctions()
    {
        _eventProcedure.Invoke();
    }
}
