﻿using SVGConverter;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class PaintingReload : MonoBehaviour
{
    [HideInInspector] public PaintingObjectData PaintingObjectData;

    public void Reload( )
    {
        foreach (Transform child in transform)
        {

            DestroyImmediate(child.gameObject);
        }

        //for (int i = transform.childCount - 1; i >= 0; i++)
        //{
        //    DestroyImmediate(transform.GetChild(i).gameObject);
        //}

        SVGToVectors.BezierSegments = PaintingObjectData.paintingSetting.BezierSegments;
        SVGToVectors.ElipseSegments = PaintingObjectData.paintingSetting.ElipseSegments;
        var shapes = SVGToVectors.Svg(PaintingObjectData.painting.File);
        var ColorElements = MeshBuilder.GenerateMesh(shapes.vectors, shapes.colors, PaintingObjectData.paintingSetting.BorderWidth, PaintingObjectData.paintingSetting.LittleFragmentArea, null, PaintingObjectData.Borders);
        foreach (var item in ColorElements.Values)
        {
            foreach (var gm in item)
            {
                gm.transform.SetParent(transform);
            }
        }
    }

   public void ReloafPref()
    {
        foreach (Transform child in transform)
        {
            DestroyImmediate(child.gameObject);
        }
        SVGToVectors.BezierSegments = PaintingObjectData.paintingSetting.BezierSegments;
        SVGToVectors.ElipseSegments = PaintingObjectData.paintingSetting.ElipseSegments;

    }
}
