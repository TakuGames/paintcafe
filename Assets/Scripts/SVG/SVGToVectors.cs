﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using UnityEngine;

namespace SVGConverter
{
    public static class SVGToVectors
    {
        #region Fields
        private static int _bezierSegments = 20;
        private static int _elipseSegments = 60;
        #endregion

        #region Properties
        public static int BezierSegments { get => _bezierSegments; set => _bezierSegments = value; }
        public static int ElipseSegments { get => _elipseSegments; set => _elipseSegments = value; }
        #endregion

        public static (List<List<Vector2>> vectors, List<Color> colors, Vector4 bounds) Svg(string text)
        {
            List<List<Vector2>> shapes = new List<List<Vector2>>();
            List<Color> colors = new List<Color>();

            var tags = Tagged(text);
            Vector4 size = Vector4.zero;

            #region LocalFunction
            void GetPath(List<Vector2> path, string tag)
            {
                var list = ToGlobalCoordinate(path, Vector2.zero, size.z, size.w);
                if (list.Count > 2)
                {
                    //if (list[0] != list[list.Count - 1]) list.Add(list[0]);

                    shapes.Add(list);
                    colors.Add(GetSolidColorBrush(GetParametr("fill", tag) ?? "#000000"));
                }
            }
            #endregion

            foreach (string tag in tags)
            {
                if (tag.StartsWith("svg"))
                {
                    var sizeS = GetParametr("viewBox", tag);
                    var sizeArrayS = sizeS.Split(new char[] { ' ' });
                    size = new Vector4(GetNumber(sizeArrayS[0]), GetNumber(sizeArrayS[1]), GetNumber(sizeArrayS[2]), GetNumber(sizeArrayS[3]));
                }
                else
                if (tag.StartsWith("path"))
                {
                    var pathComands = GetParametr("d", tag);
                    GetPath(MakePath(pathComands), tag); 
                }
                else if (tag.StartsWith("polygon"))
                {
                    var pathComands = GetParametr("points", tag);
                    GetPath(GetPolygonPoints(pathComands), tag);
                }
                else if (tag.StartsWith("rect"))
                {
                    GetPath(GetRectPoints(tag), tag);
                }
                else if (tag.StartsWith("circle"))
                {
                    var r = Convert.ToSingle(GetParametr("r", tag));
                    GetPath(GetEllipsePoints(tag, r, r), tag);
                }
                else if (tag.StartsWith("ellipse"))
                {
                    var rx = GetNumber(GetParametr("rx", tag));
                    var ry = GetNumber(GetParametr("ry", tag));
                     GetPath(GetEllipsePoints(tag, rx, ry), tag);
                }
            }
            return (shapes, colors,size);
        }

        public static Vector4 GetSvgBounds(string text)
        {
            List<List<Vector2>> shapes = new List<List<Vector2>>();
            List<Color> colors = new List<Color>();

            var tags = Tagged(text);
            Vector4 size = Vector4.zero;
            foreach (string tag in tags)
            {
                if (tag.StartsWith("svg"))
                {
                    var sizeS = GetParametr("viewBox", tag);
                    var sizeArrayS = sizeS.Split(new char[] { ' ' });
                    size = new Vector4(GetNumber(sizeArrayS[0]), GetNumber(sizeArrayS[1]), GetNumber(sizeArrayS[2]), GetNumber(sizeArrayS[3]));
                    break;
                }
                
            }

            return size;
        }


        public static Color GetSolidColorBrush(string hex)
        {
            if (hex == "none")
                return Color.black;
            hex = hex.Replace("#", string.Empty);
            byte r = (byte)(Convert.ToUInt32(hex.Substring(0, 2), 16));
            byte g = (byte)(Convert.ToUInt32(hex.Substring(2, 2), 16));
            byte b = (byte)(Convert.ToUInt32(hex.Substring(4, 2), 16));
            Color myBrush = new Color(r / 255f, g / 255f, b / 255f);
            return myBrush;
        }

        public static List<List<Vector2>> GetSvgList(string filePath)
        {
            return Svg(filePath).vectors;
        }


        private static string GetParametr(string paramName, string comand)
        {
            paramName += "=" + "\"";
            var ind1 = comand.IndexOf(paramName) ;

            if (ind1 < 0) return null;
            ind1 += paramName.Length;


            var ind2 = comand.Substring(ind1).IndexOf("\"") ;

            if (ind2 < 0) return null;

            ind2 += ind1;

            return comand.Substring(ind1, ind2 - ind1);
        }


        private static List<string> Tagged(string s)
        {
            List<string> result = new List<string>();

            var ind1 = s.IndexOf("<");
            while (ind1 >= 0)
            {
                var ind2 = s.IndexOf(">");
                result.Add(s.Substring(ind1 + 1, ind2 - ind1 - 1));
                s = s.Substring(ind2 + 1);
                ind1 = s.IndexOf("<");
            }
            return result;
        }

        private static string GetWord(int startInd, string text)
        {
            var ind = text.Substring(startInd).IndexOf(" ");
            return text.Substring(startInd + 1, ind - startInd);
        }

        public static List<Vector2> ToGlobalCoordinate(List<Vector2> points, Vector2 position, float width, float height)
        {
            for (int i = 0; i < points.Count; i++)
            {
                var point = points[i];
                point.x = (position.x + width / 2 - point.x) * -1;
                point.y = position.y + height / 2 - point.y;
                points[i] = point;
            }
            return points;
        }

        public static float GetNumber(string s)
        {
            return GetNumber(ref s);
        }

        public static float GetNumber(ref string s)
        {
            float number;
            float result = 0;

            int count = 1;
            if (s[0] == '-') count = 2;

            string numberS = s.Substring(0, count);

            while (Single.TryParse(numberS, NumberStyles.Float, CultureInfo.InvariantCulture, out number))
            {
                count++;
                result = number;
                if (s.Length < count)
                {
                    count--;
                    break;
                }
                numberS = s.Substring(0, count);
                if (numberS[count - 1] == '.')
                {
                    count++;
                    numberS = s.Substring(0, count);
                }

            }

            if (s[count - 1] != ',' && s.Length > 0) count--;
            s = s.Substring(count);
            return result;
        }


        #region GetShapePoints





        private static List<Vector2> GetEllipsePoints(string path, float rx, float ry)
        {
            var points = new List<Vector2>();
            var cx = GetNumber(GetParametr("cx", path));
            var cy = GetNumber(GetParametr("cy", path));

            var segmentCount = _elipseSegments;
            for (int i = 0; i <= segmentCount; i++)
            {
                var x = Mathf.Cos(Mathf.Lerp(0, Mathf.PI * 2, i * 1f / segmentCount)) * rx;
                var y = Mathf.Sin(Mathf.Lerp(0, Mathf.PI * 2, i * 1f / segmentCount)) * ry;
                points.Add(new Vector2(cx + x, cy + y));
            }
            return points;
        }

        private static List<Vector2> GetRectPoints(string path)
        {
            var points = new List<Vector2>();
            points.Add(new Vector2(GetNumber(GetParametr("x", path)), GetNumber(GetParametr("y", path))));
            points.Add(new Vector2(GetNumber(GetParametr("width", path)), 0) + points[0]);
            points.Add(new Vector2(points[1].x, GetNumber(GetParametr("height", path))));
            points.Add(new Vector2(points[0].x, points[2].y));
            return points;
        }

        private static List<Vector2> GetPolygonPoints(string path)
        {
            List<Vector2> points = new List<Vector2>();

            path = path.Replace(' ', ',');
            while (path.Length > 0)
            {
                points.Add(new Vector2(GetNumber(ref path), GetNumber(ref path)));
                if (path.Length > 0)
                    while (!Int32.TryParse(path[0].ToString(), out int num))
                    {
                        path = path.Substring(1);
                        if (path.Length == 0)
                            break;
                    }
            }
            return points;
        }



        #region Path Commands

        public static List<Vector2> MakePath(string pathS)
        {
            List<Vector2> points = new List<Vector2>();
            bool pathEnd = false;
            Vector2 lastPoint = Vector2.zero;
            Vector2[] bezierPoints = new Vector2[4];
            bool bezier = false;

            void Reset()
            {
                if (points.Count > 0)
                    lastPoint = points[points.Count - 1];
                bezier = false;
            }


            while (pathS.Length > 0)
            {
                char comand = pathS[0];
                pathS = pathS.Substring(1);

                switch (comand)
                {
                    case 'M':
                    case 'L':
                        points.Add(PathComands.MoveToPoint(GetNumber(ref pathS), GetNumber(ref pathS)));
                        Reset();
                        break;
                    case 'm':
                    case 'l':
                        points.Add(PathComands.MoveTo(lastPoint, GetNumber(ref pathS), GetNumber(ref pathS)));
                        Reset();
                        break;
                    case 'v':
                        points.Add(PathComands.VerticalLine(lastPoint, GetNumber(ref pathS)));
                        Reset();
                        break;
                    case 'h':
                        points.Add(PathComands.HorizontalLine(lastPoint, GetNumber(ref pathS)));
                        Reset();
                        break;
                    case 'V':
                        points.Add(PathComands.VerticalLineToPoint(lastPoint, GetNumber(ref pathS)));
                        Reset();
                        break;
                    case 'H':
                        points.Add(PathComands.HorizontalLineToPoint(lastPoint, GetNumber(ref pathS)));
                        Reset();
                        break;

                    case 'C':
                    case 'c':
                    case 'S':
                    case 's':
                    case 'Q':
                    case 'q':
                        points.AddRange(MakePathBezier(comand, bezier, ref pathS, ref lastPoint, bezierPoints));
                        bezier = true;
                        break;

                    case 'Z':
                    case 'z':
                        pathEnd = true;
                        break;
                }

                if (pathEnd) break;
            }

            return points;
        }


        private static List<Vector2> MakePathBezier(char command, bool bezierPrev, ref string pathS, ref Vector2 lastPoint, Vector2[] bezierPoints)
        {
            bezierPoints[0] = lastPoint;
            List<Vector2> bezierPointsList = new List<Vector2>();
            int segmentBezierCount = _bezierSegments;



            void SetBezierControlPoint(int countPoint, Vector2 vector, ref string path, bool offset = false)
            {
                if (countPoint > 4)
                    throw new ArgumentException("countPoint should be less or equal 4");


                for (int i = 4 - countPoint; i < 4; i++)
                {
                    if (offset)
                        bezierPoints[i] = PathComands.MoveTo(vector, GetNumber(ref path), GetNumber(ref path));
                    else
                        bezierPoints[i] = new Vector2(GetNumber(ref path), GetNumber(ref path));
                }
            }


            void SetReverseControlPoint()
            {
                if (bezierPrev)
                    bezierPoints[1] = bezierPoints[2];
                else
                    bezierPoints[1] = (bezierPoints[2] - bezierPoints[3]) * -1 + bezierPoints[3];
            }


            switch (command)
            {
                case 'C':
                    SetBezierControlPoint(3, lastPoint, ref pathS);
                    bezierPointsList = (PathComands.CreatePointsBezier(segmentBezierCount, bezierPoints));
                    break;

                case 'c':
                    SetBezierControlPoint(3, lastPoint, ref pathS, true);
                    bezierPointsList = (PathComands.CreatePointsBezier(segmentBezierCount, bezierPoints));
                    break;

                case 'S':
                    SetReverseControlPoint();

                    SetBezierControlPoint(2, lastPoint, ref pathS);
                    bezierPointsList = (PathComands.CreatePointsBezier(segmentBezierCount, bezierPoints));
                    break;

                case 's':
                    SetReverseControlPoint();

                    SetBezierControlPoint(2, lastPoint, ref pathS, true);
                    bezierPointsList = (PathComands.CreatePointsBezier(segmentBezierCount, bezierPoints));
                    break;

                case 'Q':
                    SetBezierControlPoint(2, lastPoint, ref pathS);
                    bezierPointsList = (PathComands.CreatePointsBezier(segmentBezierCount, bezierPoints[0], bezierPoints[2], bezierPoints[3]));
                    break;

                case 'q':
                    SetBezierControlPoint(2, lastPoint, ref pathS, true);
                    bezierPointsList = (PathComands.CreatePointsBezier(segmentBezierCount, bezierPoints[0], bezierPoints[2], bezierPoints[3]));
                    break;

                case 'T':
                    SetReverseControlPoint();
                    SetBezierControlPoint(1, lastPoint, ref pathS);
                    bezierPointsList = (PathComands.CreatePointsBezier(segmentBezierCount, bezierPoints[0], bezierPoints[1], bezierPoints[3]));
                    break;

                case 't':
                    SetReverseControlPoint();
                    SetBezierControlPoint(1, lastPoint, ref pathS, true);
                    bezierPointsList = (PathComands.CreatePointsBezier(segmentBezierCount, bezierPoints[0], bezierPoints[1], bezierPoints[3]));
                    break;

                default:
                    throw new ArgumentException("Svg Convertor not contain this command");
            }
            lastPoint = bezierPointsList[bezierPointsList.Count - 1];
            return bezierPointsList;
        }


        #endregion
        #endregion
    }
}