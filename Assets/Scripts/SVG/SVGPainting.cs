﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu()]
public class SVGPainting : ScriptableObject
{
    [SerializeField] public string File;   
}
