﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Money : MonoBehaviour
{
    private const string _MONEYKEY = "MONEYKey";


    [SerializeField] private TextMeshProUGUI _countText;
    [SerializeField] private int _moneyFromPaint;


    [SerializeField] private int _moneyCount;


    private void OnEnable()
    {
        GameManager.instance.SetSingletonComponent(this);
    }

    // Start is called before the first frame update
    void Start()
    {
        



        if (PlayerPrefs.HasKey(_MONEYKEY))
            ChangeMoney(PlayerPrefs.GetInt(_MONEYKEY));
        else
            ChangeMoney(0);
    }



    public void ChangeMoney(int money)
    {
        if (money < 0) money = 0;

        _moneyCount = money;
        _countText.text = money.ToString();
        PlayerPrefs.SetInt(_MONEYKEY, _moneyCount);
    }

    public void AddMoney(int money)
    {
        ChangeMoney(money + _moneyCount);
    }

    public void ClosePaint()
    {
        AddMoney(_moneyFromPaint);
    }

    private void OnApplicationQuit()
    {
        ///Test 
        PlayerPrefs.SetInt(_MONEYKEY, 50);
        ///
    }
}
