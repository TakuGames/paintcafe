﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;

public class PaintingFragment : MonoBehaviour
{

    

    public Color _color;
    private int _colorIndex;
    private Mesh _mesh;
    private List<Vector2> _vectors;
    private MeshRenderer _mr;
    private GameObject _border;
    private int _index;
    private Bounds _bounds;
    private GameObject _colorIndText;
    private float _textNumberSize;
    

    public Color color { get => _color; set => _color = value; }
    public Mesh mesh { get => _mesh; set => _mesh = value; }
    public List<Vector2> vectors { get => _vectors; set => _vectors = value; }
    public GameObject border { get => _border; set => _border = value; }
    public int index { get => _index; set => _index = value; }
    public bool click { get => _click; set => _click = value; }
    public Bounds bounds { get => _bounds; set => _bounds = value; }

    public GameObject ColorIndText { get => _colorIndText; set => _colorIndText = value; }

    public int ColorIndex {
        get => _colorIndex;
        set
        {
            _colorIndText.GetComponent<TextMeshPro>().text = value.ToString();
        }
    }

    public float TextNumberSize { get => _textNumberSize; set => _textNumberSize = value; }

    private bool _click;



    private void Start()
    {
        GameManager.instance.GetSingletonComponent<PinchZoom>().ChangeCameraSizeEvent += HideNumbers;
        _click = false;
        _bounds = GetBounds(_vectors.ToArray());

    }

    private void OnMouseUp()
    {
        if (!click)
        {
            if (!global::Click.PaintingPossible) return; 

            if (GameManager.instance.GetSingletonComponent<PaintingController>().CurrentColor != _color) return;

            global::Click.PaintingPossible = false;

            Fill();
        }
    }

    public void Fill()
    {
        if (!click)
        {
            GameManager.instance.GetSingletonComponent<PaintingController>().FragmentColoring(color, this, _index);
            Destroy(border);
            Destroy(_colorIndText);
            click = true;
        }
    }


    public struct Bounds
    {
        public Vector2 min;
        public Vector2 max;
        public Vector2 center;
        public Vector2 size;
    }

    private  Bounds GetBounds(Vector2[] points)
    {
        Bounds bounds = new Bounds();
        bounds.max = bounds.min = Vector2.zero;
        bounds.max.x = points.Max(vert => vert.x);
        bounds.max.y = points.Max(vert => vert.y);
        bounds.min.x = points.Min(vert => vert.x);
        bounds.min.y = points.Min(vert => vert.y);
        bounds.size = new Vector2(Mathf.Abs(bounds.min.x - bounds.max.x), Mathf.Abs(bounds.min.y - bounds.max.y));
        bounds.center.x = bounds.min.x + bounds.size.x / 2f;
        bounds.center.y = bounds.min.y + bounds.size.y / 2f;
        return bounds;
    }

    //private void OnDestroy()
    //{
    //    GameManager.instance.GetSingletonComponent<PinchZoom>().ChangeCameraSizeEvent -= HideNumbers;
    //}
    //private void OnDisable()
    //{
    //    GameManager.instance.GetSingletonComponent<PinchZoom>().ChangeCameraSizeEvent -= HideNumbers;
    //}

    private void HideNumbers(float curSize, float max, float min)
    {
        //bool hide = (TextNumberSize / (max - curSize)) * (max / TextNumberMax) < 0.1f;
        //Debug.Log((TextNumberSize / (max - curSize)) * (max / TextNumberMax));
        //if (_colorIndText.activeSelf != hide)
        //    _colorIndText.SetActive(hide);
    }
}
