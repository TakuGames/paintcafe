﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class VectorExtensions 
{
    public static List<Vector3> ToVector3(this List<Vector2> v2List)
    {
        var v3List = new List<Vector3>();
        foreach (var v2 in v2List)
        {
            v3List.Add(v2);
        }
        return v3List;
    }

    public static List<Vector2> ToVector2(this List<Vector3> v3List)
    {
        var v2List = new List<Vector2>();
        foreach (var v3 in v3List)
        {
            v2List.Add(v3);
        }
        return v2List;
    }

    public static Vector2 ToVector2(this Vector3 v3)
    {
        return new Vector2(v3.x,v3.y);
    }

    public static List<Vector3> ToVector3(this List<Vector2> v2List,float z)
    {
        var v3List = new List<Vector3>();
        foreach (var v2 in v2List)
        {
            v3List.Add(new Vector3(v2.x,v2.y,z));
        }
        return v3List;
    }


    public static string ArrayToString(this Vector3[] array)
    {
        string s = "";
        foreach (var el in array)
        {
            s += "new Vector3(" + el.x + "," + el.y + "," + el.z + ") , ";
        }
        return s;
    }

    public static string ArrayToString(this Vector2[] array)
    {
        string s = "";
        foreach (var el in array)
        {
            s += "(" + el.x + ";" + el.y +  ") , ";
        }
        return s;
    }

    public static Vector3 ToVector3(this Vector2 vector2, float z = 0)
    {
        Vector3 vector3 = vector2;
        vector3.z = z;
        return vector3;
    }
}
