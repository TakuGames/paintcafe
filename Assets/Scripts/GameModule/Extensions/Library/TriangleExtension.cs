﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class TriangleExtension
{
    public static float MinDist(this TriangelInfo triange, Vector3[] verts)
    {
        float min = 1000000f;

        foreach (var point in verts)
        {
            var distance = Vector3.Distance(triange.Centroid, point);
            if (distance < min)
                min = distance;
        }

        return min;
    }
}

public struct TriangelInfo
{
    public bool Border;
    public float S;

    public Vector3 A;
    public Vector3 B;
    public Vector3 C;

    public Vector3 AB;
    public Vector3 BC;
    public Vector3 CA;

    public Vector2 Centroid;
    public float minDist;
    public float Radius;
}
