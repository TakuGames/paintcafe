﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Click : MonoBehaviour
{
    [SerializeField] private Camera camera;
    [SerializeField] private float touchDistance = 0.5f;
    [SerializeField] private float delay = 0.1f;
    [SerializeField] private float _distanceToPaintFragment;
    [SerializeField][Range(0.002f,0.5f)] private float _distanceFragOrtograpficSize;

    private static bool _paintingPossible;
    private static bool _stopGame;
    private Vector3 fT;
    private float curTime = 0;

    public static bool PaintingPossible { get => _paintingPossible && !_stopGame; set => _paintingPossible = value; }
    public static bool StopGame { get => _stopGame; set => _stopGame = value; }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            PaintingPossible = true;
            fT = camera.ScreenToWorldPoint(Input.mousePosition);
        }
        if (Input.touchCount == 2)
        {
            PaintingPossible = false;
        }
        else
        {
            if (Input.GetMouseButton(0))
            {
                if (Vector3.Distance(fT, camera.ScreenToWorldPoint(Input.mousePosition)) > touchDistance)
                    PaintingPossible = false;
            }

        }

        if (Input.GetMouseButtonUp(0))
        {
            if (PaintingPossible)
            {
                var fragment = GameManager.instance.GetSingletonComponent<PaintingController>().GetNearestFragment(camera.ScreenToWorldPoint(Input.mousePosition), Camera.main.orthographicSize * _distanceFragOrtograpficSize);

                if (fragment == null) return;

                PaintingPossible = false;

                fragment.Fill();
            }

        }
    }




}
