﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;


public class PinchZoom : MonoBehaviour
{
    public delegate void ChangeCameraSizeDelegate(float curSize, float max, float min);

    #region Fields
    [SerializeField] private float _minSize;
    [SerializeField] private float _maxSize;
    [SerializeField] private Camera _uiCamera;
    [SerializeField] private Camera _textCamera;
    [SerializeField] private float _speed;
    [SerializeField] private float _maxFor1fspeed;


    private bool _wasZoom;
    private Vector4 _boundsPainting;
    public float perspectiveZoomSpeed = 0.5f;        
    public float orthoZoomSpeed = 0.5f;        
    private Camera _camera;
    private Vector3 touchStart;

    public ChangeCameraSizeDelegate ChangeCameraSizeEvent;

    private Vector3 _startMotionPos;
    private Vector3? _scalePointPosition = null;
    private bool _doPinch = false;
    private float _prevSize;
    private float startFar;

    #endregion

    private void Awake()
    {
        _camera = GetComponent<Camera>();
    }

    private void OnEnable()
    {

        GameManager.instance.SetSingletonComponent<PinchZoom>(this);
    }

    private void OnDisable()
    {
        GameManager.instance.RemoveSingletonComponent<PinchZoom>(this);
    }

    private void Start()
    {
        

        _maxSize = _camera.orthographicSize;

        _wasZoom = false;

        _boundsPainting = GameManager.instance.GetSingletonComponent<PaintingController>().Bounds;
        _boundsPainting.x = _boundsPainting.z / 2 * -1;
        _boundsPainting.y = _boundsPainting.w / 2 * -1;
        _boundsPainting.z = _boundsPainting.z / 2;
        _boundsPainting.w = _boundsPainting.w / 2;

        _doPinch = false;

        _textCamera.orthographicSize = _camera.orthographicSize;
        _textCamera.transform.position = gameObject.transform.position;
        startFar = _textCamera.farClipPlane;
    }

    private bool IsPointerOverGameObject(int fingerId)
    {
        EventSystem eventSystem = EventSystem.current;
        return (eventSystem.IsPointerOverGameObject(fingerId)
            && eventSystem.currentSelectedGameObject != null);
    }

    void Update()
    {
   
        if (Input.GetMouseButtonDown(0))
        {
            if (EventSystem.current.IsPointerOverGameObject())
            {
                _doPinch = true;

            }
            if (Input.touchCount > 0)
            {
                if (IsPointerOverGameObject(0))
                {
                    _doPinch = true;

                }
                else
                    _doPinch = false;
            }
            touchStart = _camera.ScreenToWorldPoint(Input.mousePosition);
        }
        if (Input.GetMouseButtonUp(0))
        {
            
            _doPinch = false;
        }
            // If there are two touches on the device...
        if (Input.touchCount == 2)
        {
            _doPinch = true;


            // Store both touches.
            Touch touchZero = Input.GetTouch(0);
            Touch touchOne = Input.GetTouch(1);

            // Find the position in the previous frame of each touch.
            Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
            Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

            

            // Find the magnitude of the vector (the distance) between the touches in each frame.
            float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
            float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;

            // Find the difference in the distances between each frame.
            float deltaMagnitudeDiff = prevTouchDeltaMag - touchDeltaMag;

            // If the camera is orthographic...
            if (_camera.orthographic)
            {
                // ... change the orthographic size based on the change in distance between the touches.
                _camera.orthographicSize += deltaMagnitudeDiff * (orthoZoomSpeed * (  _maxSize /  _maxFor1fspeed));

                _camera.orthographicSize = Mathf.Clamp(_camera.orthographicSize, _minSize, _maxSize);
                _textCamera.orthographicSize = _camera.orthographicSize;

                (float width, float height) cameraBounds = (_camera.orthographicSize * 2 * _camera.aspect, _camera.orthographicSize * 2);

                Vector3 pos = _scalePointPosition ?? gameObject.transform.position;
                if (_scalePointPosition == null)
                {
                    _startMotionPos = gameObject.transform.position;
                    var scalePoint = Vector3.Lerp(_uiCamera.ScreenToWorldPoint(touchZero.position), _uiCamera.ScreenToWorldPoint(touchOne.position), 0.5f).ToVector2();
                    pos.x = Mathf.Clamp(scalePoint.x, _boundsPainting.x + cameraBounds.width / 8, _boundsPainting.z - cameraBounds.width / 8);
                    pos.y = Mathf.Clamp(scalePoint.y, _boundsPainting.y + cameraBounds.height / 8, _boundsPainting.w - cameraBounds.height / 8);
                    _scalePointPosition = pos;
                    _prevSize = _camera.orthographicSize - _minSize;
                }

                var motion =  (_prevSize - (_camera.orthographicSize - _minSize)) / (_prevSize == 0 ? 1 :_prevSize);

                gameObject.transform.position = Vector3.Lerp(_startMotionPos, pos, motion);
                _textCamera.transform.position = gameObject.transform.position;
                _textCamera.farClipPlane = startFar +  (_maxSize - _camera.orthographicSize) * (FragmentNumbers.TextNumberMax /( (_maxSize - _minSize) * 1.3f) );

                ChangeCameraSizeEvent(_camera.orthographicSize, _maxSize, _minSize);
            }
            else
            {
                // Otherwise change the field of view based on the change in distance between the touches.
                _camera.fieldOfView += deltaMagnitudeDiff * perspectiveZoomSpeed;

                // Clamp the field of view to make sure it's between 0 and 180.
                _camera.fieldOfView = Mathf.Clamp(_camera.fieldOfView, 0.1f, 179.9f);
            }
            _wasZoom = true;
        }
        else
        {
            _scalePointPosition = null;

            if (_wasZoom)
            {
                _uiCamera.transform.position = transform.position;
                _uiCamera.orthographicSize = _camera.orthographicSize;
            }
            if (Input.GetMouseButton(0) && !_doPinch)
            {
                Vector3 direction = touchStart - _camera.ScreenToWorldPoint(Input.mousePosition);

                (float width, float height) cameraBounds = (_camera.orthographicSize * 2 * _camera.aspect, _camera.orthographicSize * 2);
                Vector3 pos = gameObject.transform.position;
                var target = pos + direction;
                if (target.x > _boundsPainting.x + cameraBounds.width /4 && target.x < _boundsPainting.z - cameraBounds.width /4)
                    pos.x += direction.x;
                if (target.y > _boundsPainting.y + cameraBounds.height / 4  && target.y < _boundsPainting.w - cameraBounds.height / 4 )
                    pos.y += direction.y;


                transform.position = pos;
                _textCamera.transform.position = transform.position;
            }
        }
    }

   

    public void CameraToPoint(Vector2 point, float width)
    {
        Click.StopGame = true;
        var startSize = _camera.orthographicSize * 2 * _camera.aspect;
        var endSize = width > _minSize ? width : _minSize;

        StartCoroutine(MotionToPoint(transform.position, point, startSize, endSize));
    }

    private IEnumerator MotionToPoint(Vector2 startPoint, Vector2 endPoint, float startSize, float endSize)
    {
        float cur = 0;
        var wait = new WaitForEndOfFrame();
        while(cur < 1)
        {
            _camera.orthographicSize = Mathf.Lerp(startSize, endSize, cur);
            _textCamera.orthographicSize = _camera.orthographicSize;
            transform.position = Vector2.Lerp(startPoint, endPoint, cur).ToVector3(transform.position.z);
            _textCamera.transform.position = gameObject.transform.position;
            cur += Time.deltaTime;
            yield return wait;
        }
        Click.StopGame = false;
    }


    public void SetSize(float width)
    {
        _camera.orthographicSize = (width + 100) / 2 / _camera.aspect;   
    }
}
