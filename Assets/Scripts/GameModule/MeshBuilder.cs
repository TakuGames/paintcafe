﻿using System.Collections.Generic;
using System.Collections;
using System.Linq;
using TriangleNet;
using TriangleNet.Geometry;
using UnityEngine;
using UnityEditor;
using SVGConverter;
using TMPro;

public static class MeshBuilder
{

    public static Material MeshMaterial;
    public static Material BorderMaterial;




    public static Dictionary<Color, List<PaintingFragment>> GenerateMesh(List<List<Vector2>> shapes, List<Color> colors, float width, float area, RenderTexture tex, List<MeshSubstitute> borderMeshes)
    {
        if (tex != null)
            MeshMaterial.SetTexture("_MainTex", tex);
        Dictionary<Color, List<PaintingFragment>> colorElements = new Dictionary<Color, List<PaintingFragment>>();

        var count = shapes.Count;
        List<ShapeColor> shapeColors = new List<ShapeColor>();
        for (int i = 0; i < shapes.Count; i++)
        {
            shapeColors.Add(new ShapeColor { color = colors[i], shapes = shapes[i] });
        }

        shapeColors = shapeColors.Where(sc => area < GetArea(sc.shapes.ToArray())).ToList();

        //for (int i = shapes.Count - 1; i >= 0 ; i--)
        //{
        //    if(area < GetArea(shapes[i].ToArray()))
        //    {
        //        shapes.RemoveAt(i);
        //        colors.RemoveAt(i);
        //    }
        //}

        for (int i = 0; i < shapeColors.Count; i++)
        {
            //if (area > GetArea(shapes[i].ToArray())) continue;
            PaintingFragment go;

            if (borderMeshes.Count > 0 && borderMeshes.Count == shapeColors.Count)
                go = GenerateMesh(shapeColors[i].shapes, shapeColors[i].color, shapeColors.Count - i - 1, width, shapeColors.Count/*count*/, borderMeshes[i]);
            else
                go = GenerateMesh(shapeColors[i].shapes, shapeColors[i].color, shapeColors.Count - i - 1, width, shapeColors.Count /*count*/);

            if (!colorElements.ContainsKey(shapeColors[i].color))
            {

                colorElements.Add(shapeColors[i].color, new List<PaintingFragment>());
            }
            colorElements[shapeColors[i].color].Add(go);
        }
        return colorElements;
    }

    //public static void ChangeTex(RenderTexture tex)
    //{
    //    MeshMaterial.SetTexture("_MainTex",tex);
    //}

    private static float GetArea(Vector2[] vertices)
    {
        (Vector2 max, Vector2 min, Vector2 size) bounds;
        bounds.max = bounds.min = Vector2.zero;
        bounds.max.x = vertices.Max(vert => vert.x);
        bounds.max.y = vertices.Max(vert => vert.y);
        bounds.min.x = vertices.Min(vert => vert.x);
        bounds.min.y = vertices.Min(vert => vert.y);
        bounds.size = new Vector2(Mathf.Abs(bounds.min.x - bounds.max.x), Mathf.Abs(bounds.min.y - bounds.max.y));
        return bounds.size.x * bounds.size.y;
    }

    public static PaintingFragment GenerateMesh(List<Vector2> shape, Color color, int ind, float width, int count, MeshSubstitute? borderMeshes = null)
    {
        Vector2 v;
        if (shape[0] == shape[shape.Count - 1])
            v = shape[shape.Count - 2];
        else v = shape[shape.Count - 1];

        List<Vector2> shapePoints = new List<Vector2>();
        for (int i = 0; i < shape.Count; i++)
        {
            if (i != 0)
            {
                v = shape[i - 1];
            }
            shapePoints.Add(BorderCreator.GetLeftVector(v, shape[i], 0.01f));
            //shapePoints.Add(shape[i]);
        }

        //if (shapePoints[0] != shapePoints[shapePoints.Count - 1]) shapePoints[shapePoints.Count - 1]  = shapePoints[0];


        Polygon poly = new Polygon();
        poly.Add(shapePoints);

        var triangleNetMesh = (TriangleNetMesh)poly.Triangulate();

        GameObject go = new GameObject("Generated mesh");
        go.isStatic = true;
        var mf = go.AddComponent<MeshFilter>();
        var mesh = triangleNetMesh.GenerateUnityMesh();

        int countSqrt = (int)Mathf.Sqrt(count) + 1;
        Vector2 indV = new Vector2((count - 1 - ind) / countSqrt, (count - 1 - ind) % countSqrt);

        var uv = UvBorderGenerator.CreateUVRectInOneTexture(mesh.vertices, indV, 1f / countSqrt);




        mesh.uv = uv;

        mf.mesh = mesh;

        var mr = go.AddComponent<MeshRenderer>();
        mr.sharedMaterial = MeshMaterial;
        var painting = go.AddComponent<PaintingFragment>();
        painting.mesh = mesh;

        painting.vectors = shape;

        painting.color = color;
        painting.index = (count - 1 - ind);



        go.transform.Translate(new Vector3(0f, 0f, ind / 100f));

        GameObject bord;

        if (borderMeshes == null)
            bord = BorderCreator.CreateBorder(shape, width);
        else
            bord = BorderCreator.CreateBorder(borderMeshes.Value.verts, borderMeshes.Value.triangles);
        bord.transform.SetParent(go.transform);

        painting.border = bord;



        var polygon = go.AddComponent<MeshCollider>();


        return painting;
    }




    public struct ShapeColor
    {
        public List<Vector2> shapes;
        public Color color;
    }


    
}
