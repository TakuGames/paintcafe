﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class BorderCreator 
{
    public static Vector2 GetLeftVector(Vector2 from, Vector2 to, float width)
    {
        Vector2 vector = (to - from);
        return to + new Vector2(-vector.y, vector.x).normalized * (width / 2f);
    }

    public static Vector2 GetRightVector(Vector2 from, Vector2 to, float width)
    {
        Vector2 vector = (to - from);
        return to + new Vector2(vector.y, -vector.x).normalized * (width / 2f);
    }



    public static GameObject CreateBorder(List<Vector2> shape, float width)
    {
        GameObject go = new GameObject("Border");

        List<Vector2> vert = new List<Vector2>();
        List<Vector2> points = shape;
        List<int> tri = new List<int>();

        //P2PAlgorithm(vert, points, tri, width);
        LineWithConnectorAlgorithm(vert, points, tri, width);


        var mf = go.AddComponent<MeshFilter>();
        var mr = go.AddComponent<MeshRenderer>();
        var mesh = new Mesh();
        mesh.vertices = vert.ToVector3().ToArray();
        mesh.triangles = tri.ToArray();
        mf.mesh = mesh;
        //mf.mesh.RecalculateNormals();

        MeshBuilder.BorderMaterial.color = Color.gray;
        mr.material = MeshBuilder.BorderMaterial;

        return go;
    }

    public static GameObject CreateBorder(Vector3[] verts, int[] tris)
    {
        GameObject go = new GameObject("Border");
        var mf = go.AddComponent<MeshFilter>();
        var mr = go.AddComponent<MeshRenderer>();
        var mesh = new Mesh();
        mesh.vertices = verts;
        mesh.triangles = tris;
        mf.mesh = mesh;
        //mf.mesh.RecalculateNormals();

        MeshBuilder.BorderMaterial.color = Color.gray;
        mr.material = MeshBuilder.BorderMaterial;

        return go;
    }

    private static void P2PAlgorithm(List<Vector2> vert, List<Vector2> points, List<int> tri, float width)
    {
        Vector2 v;
        Vector2 prev;
        if (points[0] == points[points.Count - 1])
            v = points[points.Count - 2];
        else v = points[points.Count - 1];

        Vector2 leftVector = GetLeftVector(v, points[0], width);
        Vector2 rightVector = GetRightVector(v, points[0], width);


        vert.Add(leftVector);
        vert.Add(rightVector);
        for (int i = 0; i < points.Count; i++)
        {

            if (i == points.Count - 1)
            {
                v = points[0];

            }
            else
            {

                v = points[i + 1];
            }

            if (i + 2 > points.Count - 1)
                prev = points[(i + 2) - points.Count];
            else
                prev = points[i + 2];

            rightVector = GetRightVector(points[i], v, width);
            leftVector = GetLeftVector(points[i], v, width);

            vert.Add(leftVector);
            vert.Add(rightVector);

            tri.Add(i * 2);
            tri.Add(i * 2 + 3);
            tri.Add(i * 2 + 1);

            tri.Add(i * 2);
            tri.Add(i * 2 + 2);
            tri.Add(i * 2 + 3);
        }
    }

    private static void LineWithConnectorAlgorithm(List<Vector2> vert, List<Vector2> points, List<int> tri, float width)
    {
        int count;
        if (points[0] == points[points.Count - 1])
            count = points.Count - 1;
        else count = points.Count;

        for (int i = 0; i < count; i++)
        {
            Vector2 prevP, nextP;
            prevP = i == 0 ? points[count - 1] : points[i - 1];
            nextP = i == (count - 1) ? points[0] : points[i + 1];

            //var pointDir = new PointsDir();

            //vert[i * 5] = GetLeftVector(prevP, points[i], width);
            //vert[i * 5 + 1] = GetRightVector(prevP, points[i], width);
            //vert[i * 5 + 2] = points[i];
            //vert[i * 5 + 3] = GetLeftVector(nextP, points[i], width);
            //vert[i * 5 + 4] = GetRightVector(nextP, points[i], width);
            vert.Add(GetLeftVector(prevP, points[i], width));
            vert.Add(GetRightVector(prevP, points[i], width));
            vert.Add(points[i]);
            vert.Add(GetLeftVector(nextP, points[i], width));
            vert.Add(GetRightVector(nextP, points[i], width));

            //pointsDirs.Add(pointDir);
        }
        for (int i = 0; i < count; i++)
        {
            int nextP;
            nextP = i == (count - 1) ? 0 : i + 1;

            tri.Add(i * 5 + 4);
            tri.Add(nextP * 5);
            tri.Add(i * 5 + 3);

            tri.Add(i * 5 + 3);
            tri.Add(nextP * 5);
            tri.Add(nextP * 5 + 1);

            tri.Add(i * 5 + 2);
            tri.Add(i * 5 + 1);
            tri.Add(i * 5 + 3);

            tri.Add(i * 5 + 2);
            tri.Add(i * 5);
            tri.Add(i * 5 + 4);
        }
    }

    public struct PointsDir
    {
        public Vector2 L1;
        public Vector2 R1;
        public Vector2 C;
        public Vector2 L2;
        public Vector2 R2;
    }
}
