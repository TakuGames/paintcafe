﻿
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    //[SerializeField] private PaintingController _paintingController;
    //[SerializeField] private PaintsUI _paintsUI;
    //[SerializeField] private PinchZoom _pinchZoom;
    //[SerializeField] private IndTextController _indTextController;

    public static GameManager instance;

    //public PaintingController paintingController  => _paintingController;
    //public PaintsUI paintsUI => _paintsUI;

    //public PinchZoom pinchZoom  => _pinchZoom;

    //public IndTextController IndTextController { get => _indTextController; set => _indTextController = value; }

    private Dictionary<Type, UnityEngine.Object> _container = new Dictionary<Type, UnityEngine.Object>();

    void Awake()
    {

        if (instance == null)
        {
            instance = this;
        }
        else if (instance == this)
        {
            Destroy(gameObject);
        }

        
    }

    public T GetSingletonComponent<T>() where T : UnityEngine.Object
    {
        return (T)_container[typeof(T)];
    }

    public void SetSingletonComponent<T>(T instance) where T : UnityEngine.Object
    {
        Type type = typeof(T);
        if (_container.ContainsKey(type))
            _container[type] = instance;
        else
            _container.Add(type, instance);
            
        //_container.Add()
    }

    public void RemoveSingletonComponent<T>(T instance) where T : UnityEngine.Object
    {
        Type type = typeof(T);
        if (_container.ContainsKey(type))
            _container.Remove(type);

        //_container.Add()
    }
}
