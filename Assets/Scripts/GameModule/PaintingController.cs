﻿using SVGConverter;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

#region Event Classes
public delegate void SetColorsDelegate(Color[] colors, int[] counts);
public delegate void ChangeColorDelegate(Color color, int curCount);
#endregion

public class PaintingController : MonoBehaviour
{

    #region Events
    public event SetColorsDelegate SetColors;
    public event ChangeColorDelegate ChangeColor;
    #endregion


    #region Fields
    [SerializeField] private ComputeShader _computeShader;
    

    [Header("Painting parammeters")]
    //[SerializeField] SOPaintingsPool _soPaintings;
    [SerializeField] Texture2D _checker;
    [SerializeField] private PaintingObjectData _defaultPainting;
    public Material MeshMaterial;
    public Material BorderMaterial;
    [SerializeField] private TextMeshPro TextIndPref;

    private Dictionary<Color, List<PaintingFragment>> ColorElements;

    private int _fillKernel;
    private int _lightSelectKernel;
    private int _selectKernel;
    private RenderTexture _renderTexture;
    private Vector4 _bounds;
    private Color? _currentColor ;

    #endregion

    #region Properties
    public Vector4 Bounds { get => _bounds; set => _bounds = value; }
    public Color CurrentColor
    {
        get
        {
            return _currentColor ?? Color.white;
        } 
        set
        {
            if(_currentColor != null && ColorElements.ContainsKey(CurrentColor))                     
                foreach (var painting in ColorElements[CurrentColor])
                {
                    if(!painting.click)
                        ClearFragment(painting.index);
                }

                foreach (var painting in ColorElements[value])
                {
                    if (!painting.click)
                        SelectFragment(painting.index,Color.cyan);
                }

            

            _currentColor = value;
        }
    }
    #endregion

    #region Unity Functions

    private void OnEnable()
    {

        GameManager.instance.SetSingletonComponent<PaintingController>(this);
    }

    private void OnDisable()
    {
        GameManager.instance.RemoveSingletonComponent<PaintingController>(this);
    }

    private void Start()
    {
        //var painting = LevelSingleton.Instance.CurPainting;
        //if (painting == null)
        //    painting = _defaultPainting;
        //MeshBuilder.BorderMaterial = this.BorderMaterial;
        //MeshBuilder.MeshMaterial = this.MeshMaterial;

        //if (painting.paintingSetting.BezierSegments < 1)
        //    throw new ArgumentException("Bezier Segments should be more 1");
        //if (painting.paintingSetting.ElipseSegments < 5)
        //    throw new ArgumentException("Elipse Segments should be more 1");

        //SVGToVectors.BezierSegments = painting.paintingSetting.BezierSegments;
        //SVGToVectors.ElipseSegments = painting.paintingSetting.ElipseSegments;
        //var shapes = SVGToVectors.Svg(painting.painting.File);
        //Bounds = shapes.bounds;

        ////GameManager.instance.GetSingletonComponent<PinchZoom>().SetSize(Bounds.z);


        //ColorElements = MeshBuilder.GenerateMesh(shapes.vectors, shapes.colors, painting.paintingSetting.BorderWidth, painting.paintingSetting.LittleFragmentArea, _renderTexture, painting.Borders);

        ////InitializeComputeShaders(ColorElements.Sum(ce => ce.Value.Count));

        ////var colorsAndCounts = GetColorsAndCounts();
        ////SetColors?.Invoke(colorsAndCounts.colors, colorsAndCounts.counts);

        ////FragmentNumbers.pref = TextIndPref;
        ////FragmentNumbers.SetNumbers(ColorElements);

        ////CurrentColor = ColorElements.Keys.First();
    }
    #endregion


    #region Private Functions
    private void InitializeComputeShaders(int count)
    {
        _renderTexture = new RenderTexture(1024, 1024, 32);
        _renderTexture.enableRandomWrite = true;
        _renderTexture.Create();

        
        int countSqrt = (int)Mathf.Sqrt(count) + 1;

        var toWhite = _computeShader.FindKernel("ToWhite");
        _computeShader.SetTexture(toWhite, "Result", _renderTexture);

        _fillKernel = _computeShader.FindKernel("Filling");
        _selectKernel = _computeShader.FindKernel("Select");
        _lightSelectKernel = _computeShader.FindKernel("LightSelect");

        _computeShader.SetFloat("Length", (1024f / countSqrt));
        _computeShader.SetInt("CountSqrt", countSqrt);
        _computeShader.SetVector("BorderColor", Color.black);
        _computeShader.SetVector("CheckTexSize", new Vector2(1024, 1024));
        _computeShader.SetFloat("BorderWidthPercent", 0.01f);

        _computeShader.Dispatch(toWhite, 256, 256, 1);
    }

    private (Color[] colors, int[] counts) GetColorsAndCounts()
    {
        Color[] colors = new Color[ColorElements.Count];
        int[] counts = new int[ColorElements.Count];
        int i = 0;
        foreach (var key in ColorElements.Keys)
        {
            counts[i] = ColorElements[key].Count;
            colors[i] = key;
            i++;
        }
        return (colors, counts);
    }

    


    private void NextColor(Color prevColor)
    {
        Color? nextColor = null;
        bool isNeighbour = false;
        foreach (var color in ColorElements.Keys)
        {

            if (color == prevColor)
            {
                if (nextColor == null)
                {
                    nextColor = null;
                    isNeighbour = true;
                }
                else
                    break;
            }
            else
            {
                nextColor = color;
                if (isNeighbour) break;
            }
        }

        if (nextColor == null)
            _currentColor = null;
        else
        {
            CurrentColor = (Color)nextColor;
            GameManager.instance.GetSingletonComponent<PaintsUI>().SetCurColor(CurrentColor);
        }
        
    }


   

    private IEnumerator GameOver()
    {
        yield return new WaitForSeconds(1);
        LevelSingleton.Instance.CurPainting.isPainted = true;
        SceneManager.LoadScene(0);
    }



    #endregion


    #region Public Finctions



    public void FragmentColoring(Color color, PaintingFragment painting, int ind)
    {
        ColorElements[color].Remove(painting);

        _computeShader.SetTexture(_fillKernel, "Result", _renderTexture);
        _computeShader.SetInt("Index", ind);
        _computeShader.SetVector("Color", color);
        _computeShader.Dispatch(_fillKernel, 256, 256, 1);

        ChangeTex(_renderTexture);

        ChangeColor?.Invoke(color, ColorElements[color].Count);

        if (ColorElements[color].Count <= 0)
        {
            NextColor(color);
            ColorElements.Remove(color);

            if (ColorElements.Count <= 0)
                StartCoroutine(GameOver());
        }
        
    }

    public void SelectFragment(int ind, Color color)
    {
        _computeShader.SetTexture(_selectKernel, "Result", _renderTexture);
        _computeShader.SetInt("Index", ind);
        _computeShader.SetTexture(_selectKernel, "Checker", _checker);
        _computeShader.Dispatch(_selectKernel, 256, 256, 1);
        ChangeTex(_renderTexture);
    }

    public void SelectFragmentWithLight(int ind)
    {
        RenderTexture result = new RenderTexture(_renderTexture);
        result.enableRandomWrite = true;
        result.Create();
        _computeShader.SetTexture(_lightSelectKernel, "Result", result);
        _computeShader.SetTexture(_lightSelectKernel, "Original", _renderTexture);
        _computeShader.SetInt("Index", ind);
        _computeShader.Dispatch(_lightSelectKernel, 256, 256, 1);
        ChangeTex(result);
    }

    public void ClearFragmentWithLight()
    {
        ChangeTex(_renderTexture);
    }

    public void ClearFragment(int ind)
    {
        _computeShader.SetTexture(_fillKernel, "Result", _renderTexture);
        _computeShader.SetInt("Index", ind);
        _computeShader.SetVector("Color", Color.white);
        _computeShader.Dispatch(_fillKernel, 256, 256, 1);
        ChangeTex(_renderTexture);
    }

    //public void NextPainting()
    //{
    //    _soPaintings.NextPainting();
    //}

    

    public PaintingFragment GetNearestFragment( Vector3 point, float maxLength)
    {
        if (!ColorElements.ContainsKey(CurrentColor)) return null;

        foreach (var fragment in ColorElements[CurrentColor])
        {
            if (Vector2.Distance(fragment.mesh.bounds.center.ToVector2(), point.ToVector2()) <= maxLength)
                return fragment;
        }

        return null;
    }

    public PaintingFragment GetFirstFragmentfromCurColor()
    {
        if (!ColorElements.ContainsKey(CurrentColor)) return null;

        return ColorElements[CurrentColor][UnityEngine.Random.Range(0, ColorElements[CurrentColor].Count)];
    }


    public IEnumerator PaintAll()
    {
        var keys = ColorElements.Keys.ToArray();
        var wait = new WaitForEndOfFrame();
        for (int i = keys.Length - 1; i >= 0; i--)
        {
            var fragments = ColorElements[keys[i]];
            for (int j = fragments.Count - 1; j >= 0; j-- )
            {
                fragments[j].Fill();
                yield return wait;
            }

        }
    }

    public void ChangeTex(RenderTexture tex)
    {
        MeshMaterial.SetTexture("_MainTex", tex);
    }

    #endregion

}


