﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System.Globalization;

public class Energy : MonoBehaviour
{
    private const string _ENERGYKEY = "EnergyKey";
    private const string _LASTTIMEKEY = "EnergyLastTimeKey";

    [SerializeField] private float _timeDelay;
    [SerializeField] private int _maxEnergyCount;
    [SerializeField] private int _paintPay;

    [SerializeField] private Slider _slider;
    [SerializeField] private TextMeshProUGUI _sliderText;
    [SerializeField] private TextMeshProUGUI _timerText;

    [SerializeField]  private int _energyCount;
    private DateTime _lastTime;
    private DateTime _nextTime;
    private DateTime _curTime;

    private bool CorutineEnable;

    private void OnEnable()
    {
        GameManager.instance.SetSingletonComponent(this);
        _slider.onValueChanged.AddListener(ChangeSliderValue);
        _slider.maxValue = _maxEnergyCount;
    }

    // Start is called before the first frame update
    void Start()
    {
        

        if (PlayerPrefs.HasKey(_LASTTIMEKEY))
            _lastTime = DateTime.Parse(PlayerPrefs.GetString(_LASTTIMEKEY));
        else
            _lastTime = DateTime.Now;

        _curTime = _nextTime = DateTime.Now;

        if (PlayerPrefs.HasKey(_ENERGYKEY))
            ChangeEnergy(PlayerPrefs.GetInt(_ENERGYKEY));
        else
            ChangeEnergy(_maxEnergyCount);


        if (!CorutineEnable)
            StartCoroutine(Timer());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ChangeEnergy(int energy)
    {
        _energyCount = energy;
        PlayerPrefs.SetInt(_ENERGYKEY, _energyCount);
        _slider.value = _energyCount;

        if (_energyCount == _maxEnergyCount)
        {
            CorutineEnable = false;
            _timerText.gameObject.SetActive(false);
            StopCoroutine(Timer());
        }
        else
        {
            if (!CorutineEnable)
            {

                StartCoroutine(Timer());
            }
            _timerText.gameObject.SetActive(true);
        }
    }

    public void AddEnergy(int energy)
    {
        ChangeEnergy( Mathf.Clamp(energy + _energyCount, 0, _maxEnergyCount ));
    }

    public void SubstractEnergy(int energy)
    {
        ChangeEnergy( Mathf.Clamp(_energyCount - energy, 0, _maxEnergyCount));
    }

    private IEnumerator Timer()
    {
        CorutineEnable = true;
        var wait = new WaitForSeconds(1f);
        while (true)
        {
            _curTime = _curTime.AddSeconds(1f);
            if(_curTime > _nextTime)
            {
                var seconds = _nextTime.Subtract(_lastTime).TotalSeconds;
                _lastTime = _nextTime;
                _nextTime = _nextTime.AddSeconds(_timeDelay);

                AddEnergy((int)(seconds / _timeDelay));
            }
            var time = (_nextTime.Subtract(_curTime));
            //_timerText.text = (time.Minutes > 0 ?  (time.Minutes + ":") : "") + time.Seconds.ToString();
            //if(_timerText.gameObject.active)
                _timerText.text =  time.ToString("mm") + ":" + time.ToString("ss");
            yield return wait;           
        }
    }


    private void ChangeSliderValue(float value)
    {
        _sliderText.text = (int)value + "/" + _maxEnergyCount;
    }


    private void OnDisable()
    {
        PlayerPrefs.SetString(_LASTTIMEKEY, _lastTime.ToString());        
        _slider.onValueChanged.RemoveListener(ChangeSliderValue);
    }

    public void Pay()
    {
        SubstractEnergy(_paintPay);
    }

    private void OnApplicationQuit()
    {
        ///Test 
        PlayerPrefs.SetInt(_ENERGYKEY, 50);
        PlayerPrefs.SetString(_LASTTIMEKEY, DateTime.Now.ToString());
        ///
    }
}
