﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class PathComands 
{
    /// <summary>
    /// SVG M, L
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <returns></returns>
    public static Vector2 MoveToPoint(float x, float y)
    {
        return new Vector2(x, y);
    }


    /// <summary>
    /// SVG m, l
    /// </summary>
    /// <param name="vector"></param>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <returns></returns>
    public static Vector2 MoveTo(Vector2 vector, float x, float y)
    {
        vector.x += x;
        vector.y += y;
        return vector;
    }

    /// <summary>
    /// SVG h
    /// </summary>
    /// <param name="vector"></param>
    /// <param name="x"></param>
    /// <returns></returns>
    public static Vector2 HorizontalLine(Vector2 vector, float x)
    {
        return MoveTo(vector, x, 0);
    }

    /// <summary>
    /// SVG v
    /// </summary>
    /// <param name="vector"></param>
    /// <param name="y"></param>
    /// <returns></returns>
    public static Vector2 VerticalLine(Vector2 vector, float y)
    {
        return MoveTo(vector, 0, y);
    }

    /// <summary>
    /// SVG H
    /// </summary>
    /// <param name="x"></param>
    /// <returns></returns>
    public static Vector2 HorizontalLineToPoint(Vector2 vector, float x)
    {
        return MoveToPoint(x, vector.y);
    }

    /// <summary>
    /// SVG V
    /// </summary>
    /// <param name="y"></param>
    /// <returns></returns>
    public static Vector2 VerticalLineToPoint(Vector2 vector, float y)
    {
        return MoveToPoint(vector.x, y);
    }

    #region Bezier

     public static List<Vector2> CreatePointsBezier(int segmentCount, Vector2 p0, Vector2 p1, Vector2 p2, Vector2 p3)
    {
        List<Vector2> points = new List<Vector2>();
        float t = 0;
        for (int i = 1; i <= segmentCount; ++i)
        {
            t += 1 / (float)segmentCount;
            Vector3 pp = CalculateBezierPoint(t, p0, p1, p2, p3);
            points.Add(pp);
        }
        return points;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="segmentCount"></param>
    /// <param name="points">Get only 4 points </param>
    /// <returns></returns>
    public static List<Vector2> CreatePointsBezier(int segmentCount,params Vector2[] points)
    {
        if(Vector2.Distance(points[0],points[1]) + Vector2.Distance(points[1], points[2]) + Vector2.Distance(points[2], points[3]) < 0.1f)
            return new List<Vector2> { points[0], points[1], points[2], points[3] };

            
        return CreatePointsBezier(segmentCount, points[0], points[1], points[2], points[3]);
    }

    //static List<Vector2> CreatePointsBezier(int segmentCount, Vector3 p0, Vector3 p1, Vector3 p2)
    //{
    //    List<Vector2> points = new List<Vector2>();
    //    float t = 0;
    //    for (int i = 1; i <= segmentCount; ++i)
    //    {
    //        t += 1 / (float)segmentCount;
    //        Vector3 pp = CalculateBezierPoint(t, p0, p1, p2);
    //        points.Add(pp);
    //    }
    //    return points;
    //}

    static Vector2 CalculateBezierPoint(float t, Vector2 p0, Vector2 p1, Vector2 p2, Vector2 p3)
    {
        float u = 1 - t;
        float tt = t * t;
        float uu = u * u;
        float uuu = uu * u;
        float ttt = tt * t;

        Vector2 p = uuu * p0;    //first term
        p += 3 * uu * t * p1;    //second term
        p += 3 * u * tt * p2;    //third term
        p += ttt * p3;           //fourth term

        return p;
    }

    static Vector2 CalculateBezierPoint(float t, Vector2 p0, Vector2 p1, Vector2 p2)
    {
        Vector2 p;
        Vector2 f = Vector2.Lerp(p0, p1, t);
        Vector2 s = Vector2.Lerp(p1, p2, t);
        p = Vector2.Lerp(f, s, t);

        return p;
    }
    #endregion
}
