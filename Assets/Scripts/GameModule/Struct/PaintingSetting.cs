﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public struct PaintingSetting 
{
    
    [Range(2f, 300)] public float LittleFragmentArea;

     public int BezierSegments;
     public int ElipseSegments;
    [Range(0.2f, 5f)]  public float BorderWidth;
}
