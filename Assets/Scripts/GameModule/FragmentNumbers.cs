﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using TMPro;
using System;

public static class FragmentNumbers 
{
    public static float TextNumberMax;
    public static TextMeshPro pref;

    public static void SetNumbers(Dictionary<Color, List<PaintingFragment>> colorElements)
    {
        int count = 1;
        foreach (var item in colorElements)
        {
            foreach (var fragment in item.Value)
            {
                fragment.ColorIndText = AddNumber(fragment.mesh, fragment);
                fragment.ColorIndex = count;
            }
            count++;
        }
    }


    public static GameObject AddNumber(Mesh mesh, PaintingFragment parent)
    {
        var tri = mesh.triangles;
        List<TriangelInfo> triangles = new List<TriangelInfo>();
        var verts = mesh.vertices;
        for (int i = 0; i < tri.Length; i += 3)
        {
            var a = (mesh.vertices[tri[i]] - mesh.vertices[tri[i + 1]]);
            var b = (mesh.vertices[tri[i + 2]] - mesh.vertices[tri[i + 1]]);
            var c = (mesh.vertices[tri[i]] - mesh.vertices[tri[i + 2]]);
            var angle = Vector2.Angle(a.ToVector2(), b.ToVector2()) * (Mathf.PI / 180);
            var S = 0.5f * a.magnitude * b.magnitude * Mathf.Sin(angle);
            var border = Mathf.Abs(tri[i] - tri[i + 1]) == 1 || Mathf.Abs(tri[i + 1] - tri[i + 2]) == 1 || Mathf.Abs(tri[i + 2] - tri[i]) == 1;

            var p = 0.5f * (a.magnitude + b.magnitude + c.magnitude);
            var radius = Mathf.Sqrt((p - a.magnitude) * (p - b.magnitude) * (p - c.magnitude) / p);

            var centr = new Vector2();
            centr.x = (mesh.vertices[tri[i]].x + mesh.vertices[tri[i + 1]].x + mesh.vertices[tri[i + 2]].x) / 3f;
            centr.y = (mesh.vertices[tri[i]].y + mesh.vertices[tri[i + 1]].y + mesh.vertices[tri[i + 2]].y) / 3f;



            var triangle = new TriangelInfo { AB = a, BC = b, CA = c, S = S, Border = border, Centroid = centr, Radius = radius, A = mesh.vertices[tri[i]], B = mesh.vertices[tri[i + 1]], C = mesh.vertices[tri[i + 2]], };
            //triangle.minDist = triangle.MinDist(verts);

            triangles.Add(triangle);
        }

        

        //return InscribedCircle(triangles.ToArray(),parent);
        return MinDistanceAlghiritm(triangles.ToArray(), verts, parent);
    }

    private static GameObject InscribedCircle(TriangelInfo[] triangles, PaintingFragment parent)
    {

        float devid = 1f;
        var resultTris = triangles.OrderByDescending(tr => tr.Radius);
        float length = 0;
        do
        {

            resultTris = triangles.Where(tr => IsEmtyPlace(tr, tr.Radius * devid, parent.transform, 100f)).OrderByDescending(tr => tr.Radius); //.Take(triangles.Count / 2).OrderBy(tr => (!tr.Border));
            devid -= 0.02f;
        } while (resultTris.Count() < 1);

        TriangelInfo resultTri;
        var resultTrisBord = resultTris.Where(tr => !tr.Border);
        if (resultTrisBord.Count() > 0)
        {
            if (resultTrisBord.First().Radius > resultTris.First().Radius / 2f)
                resultTri = resultTrisBord.First();
            else
                resultTri = resultTris.First();
        }
        else
        {
            resultTri = resultTris.First();
        }


        length = resultTri.Radius * devid;
        //GameObject go = Instantiate(textNumberPref, resultTri.Centroid.ToVector3(-length), Quaternion.identity, parent.transform);
        GameObject go = CreateTMPro(resultTri.Centroid.ToVector3(-length), Quaternion.identity, parent.transform);
        Debug.Log(-length);
        go.GetComponent<RectTransform>().sizeDelta = new Vector2(length, length);
        parent.TextNumberSize = length;

        if (length > TextNumberMax)
            TextNumberMax = length;

        return go;
    }

    private static GameObject MinDistanceAlghiritm(TriangelInfo[] triangles, Vector3[] points, PaintingFragment parent)
    {
        var verts = new List<Vector3>(triangles.Where(tr => !IsEmtyPlace(tr, tr.Radius, parent.transform, 100f)).Select(tr => tr.Centroid).ToList().ToVector3());
        verts.AddRange(points);
        //verts.AddRange(points);
        //var blokTris = triangles.Where(tr => !IsEmtyPlace(tr, parent.transform, 100f, devid));
        //verts.AddRange(blokTris.Select(tr => tr.Centroid).ToList().ToVector3());
        //verts.AddRange(blokTris.Select(tr => tr.A).ToList());
        //verts.AddRange(blokTris.Select(tr => tr.B).ToList());
        //verts.AddRange(blokTris.Select(tr => tr.C).ToList());

        for (int i = 0; i < triangles.Count(); i++)
        {
            triangles[i].minDist = triangles[i].MinDist(verts.ToArray());
        }

        float devid = 1f;
        var resultTris = triangles.OrderByDescending(tr => tr.minDist);
        float length = 0;
        do
        {

            resultTris = triangles.Where(tr => IsEmtyPlace(tr, tr.minDist * devid, parent.transform, 100f)).OrderByDescending(tr => tr.minDist); 
            devid -= 0.02f;
        } while (resultTris.Count() < 1);

        var resultTri = resultTris.First(); 

        length = resultTri.minDist * devid * 0.5f;
        //GameObject go = Instantiate(textNumberPref, resultTri.Centroid.ToVector3(-length), Quaternion.identity, parent.transform);
        GameObject go = CreateTMPro(resultTri.Centroid.ToVector3(-length), Quaternion.identity, parent.transform);
        
        go.GetComponent<RectTransform>().sizeDelta = new Vector2(length, length);
        parent.TextNumberSize = length;

        if (length > TextNumberMax)
            TextNumberMax = length;

        return go;
    }



    private static bool IsEmtyPlace(TriangelInfo triangel, float length, Transform parent, float distance)
    {

        var radius = length * 1f;
        return !Physics.SphereCast(triangel.Centroid.ToVector3(parent.transform.position.z - distance), radius, Vector3.forward, out RaycastHit hit, distance - (radius + 0.0001f));
        //var objects = Physics.SphereCastAll(triangel.Centroid.ToVector3(parent.transform.position.z - distance), radius, Vector3.forward, distance ).Where(o => o.collider.gameObject.transform.position.z < parent.position.z);
        //return objects.Count() > 0;
    }

    private static GameObject CreateTMPro(Vector3 position,Quaternion rotation, Transform parent)
    {
        GameObject go = new GameObject();       
        var tmp = go.AddComponent<TextMeshPro>();

        SetPropertiesFromOldObject<TextMeshPro>( tmp, pref);

        //tmp.GetDataFromOldClass()
        //tmp.alignment = pref.alignment;
        //tmp.color = pref.color;
        //tmp.enableAutoSizing = pref.enableAutoSizing;

        go.transform.position = position;
        go.transform.rotation = rotation;
        go.transform.SetParent(parent);
        
        return go;
    }

    private static void SetPropertiesFromOldObject<T>(T newComponent, T oldComponent)
    {
        var type = typeof(T);
        foreach (var pi in type.GetProperties())
        {
            
            if (pi.GetGetMethod() != null && pi.GetSetMethod() != null)
                try 
                {
                    var value = pi.GetValue(oldComponent, null);
                    if(pi.PropertyType.IsValueType)
                        pi.SetValue(newComponent,value );
                }catch(Exception ex) { }
                
        }
    }
}
