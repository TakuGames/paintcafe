﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="Painting Pool",menuName = "Painting Pool")]
public class SOPaintingsPool : ScriptableObject
{
    private int _currPainting = 0;

    public PaintingSetting[] Paintings;

    public void NextPainting()
    {
        _currPainting++;
        if (_currPainting >= Paintings.Length)
            _currPainting = 0;
        
    }

    public PaintingSetting GetPainting()
    {
        return Paintings[_currPainting];
    }

    
}
