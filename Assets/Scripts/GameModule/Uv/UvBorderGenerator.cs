﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class UvBorderGenerator 
{
    private struct Bounds
    {
        public Vector2 min;
        public Vector2 max;
        public Vector2 size;
    }

    private static Bounds GetBounds(Vector3[] vertices)
    {
        Bounds bounds;
        bounds.max = bounds.min  = Vector2.zero;
        bounds.max.x = vertices.Max(vert => vert.x);
        bounds.max.y = vertices.Max(vert => vert.y);
        bounds.min.x = vertices.Min(vert => vert.x);
        bounds.min.y = vertices.Min(vert => vert.y);
        bounds.size = new Vector2(Mathf.Abs(bounds.min.x - bounds.max.x), Mathf.Abs(bounds.min.y - bounds.max.y));
        return bounds;
    }

    public static (int ul, int ur, int dr, int dl) GetVertAnglePointsInd(Vector3[] vertices)
    {
        var bounds = GetBounds(vertices);
        return GetVertAnglePointsInd(vertices, bounds);
    }

    private static (int ul, int ur, int dr, int dl ) GetVertAnglePointsInd(Vector3[] vertices,Bounds bounds )
    {
        int ul = -1;
        int ur = -1;
        int dr = -1;
        int dl = -1;
        int GetMinDistInd(Vector2 pointAngle)
        {
            var min = float.MaxValue;
            int ind = -1;
            for (int i = 0; i < vertices.Length; i++)
            {
                if (i == ul || i == ur || i == dr || i == dl)
                    continue;
                var distance = Vector2.Distance(pointAngle, vertices[i].ToVector2());
                if (min > distance)
                {
                    min = distance;
                    ind = i;
                }
            }
            return ind;
        }

         ul = GetMinDistInd(new Vector2(bounds.min.x, bounds.max.y));
         ur = GetMinDistInd(new Vector2(bounds.max.x, bounds.max.y));
         dr = GetMinDistInd(new Vector2(bounds.max.x, bounds.min.y));
         dl = GetMinDistInd(new Vector2(bounds.min.x, bounds.min.y));

        return (ul, ur, dr, dl);
    }

    public static (Vector2[] up, Vector2[] left, Vector2[] down, Vector2[] right) CreateRect(Vector3[] vertices)
    {
        List<Vector2> up = new List<Vector2>();
        List<Vector2> left = new List<Vector2>();
        List<Vector2> down = new List<Vector2>();
        List<Vector2> right = new List<Vector2>();

        var points = GetVertAnglePointsInd(vertices);
        int count = points.ul;

        void DirectionArraySet(List<Vector2> directionArray, int startInd, int endInd)
        {
            var length = startInd < endInd ? Mathf.Abs(startInd - endInd) : (vertices.Length  + endInd - startInd);
            for (int i = 0; i < length; i++)
            {
                directionArray.Add(vertices[count]);
                count++;
                if (count >= vertices.Length)
                    count = 0;
            }
        }


        DirectionArraySet(up, points.ul, points.ur);
        DirectionArraySet( right, points.ur, points.dr);
        DirectionArraySet(down, points.dr, points.dl);
        DirectionArraySet(left, points.dl, points.ul);

        return (up.ToArray(), left.ToArray(), down.ToArray(), right.ToArray());
        
    }

    public static Vector2[] CreateUVRect(Vector3[] vertices,  float borderWidth = 0)
    {
        return CreateUVRect(vertices, Vector2.zero, Vector2.one, 1, borderWidth);
    }
    public static Vector2[] CreateUVRect(Vector3[] vertices, Vector2 min , Vector2 max, float scale, float borderWidth = 0)
    {


        Vector2[] uvPoints = new Vector2[vertices.Length];

        var bounds = GetBounds(vertices);
        var points = GetVertAnglePointsInd(vertices,bounds);
        int count = points.ul;

        void DirectionArraySet(Vector2 vector,Vector2 directVect, int startInd, int endInd)
        {
            var length = startInd < endInd ? Mathf.Abs(startInd - endInd) : (vertices.Length + endInd - startInd);
            for (int i = 0; i < length; i++)
            {
                uvPoints[count].x = (Mathf.Abs(bounds.min.x - vertices[count].x) / bounds.size.x * scale) * directVect.x + vector.x;
                uvPoints[count].y = (Mathf.Abs(bounds.min.y - vertices[count].y) / bounds.size.y * scale) * directVect.y + vector.y;
                
                count++;
                if (count >= vertices.Length)
                    count = 0;
            }
        }

        DirectionArraySet(new Vector2(min.x + borderWidth, max.y - borderWidth), new Vector2(1, 0), points.ul, points.ur);
        DirectionArraySet(new Vector2(max.x - borderWidth, min.y + borderWidth), new Vector2(0, 1), points.ur, points.dr);
        DirectionArraySet(new Vector2(min.x + borderWidth, min.y + borderWidth), new Vector2(1, 0), points.dr, points.dl);
        DirectionArraySet(new Vector2(min.x + borderWidth, min.y + borderWidth), new Vector2(0, 1), points.dl, points.ul);

        return uvPoints;

    }

    public static (Texture2D texture, Vector2[] uv) CreateTextureAndUvRect(Vector3[] vertices,int pixelPerUnit)
    {
        
        Vector2[] uvPoints = new Vector2[vertices.Length];

        var bounds = GetBounds(vertices);
        Texture2D texture = new Texture2D((int)(bounds.size.x * pixelPerUnit), (int)(bounds.size.y * pixelPerUnit));

        for (int i = 0; i < vertices.Length - 1; i++)
        {
            var vectorStart = Vector2.zero;
            var vectorEnd = Vector2.zero;
            vectorStart.x = (Mathf.Abs(bounds.min.x - vertices[i].x) / bounds.size.x) * pixelPerUnit;
            vectorStart.y = (Mathf.Abs(bounds.min.x - vertices[i].y) / bounds.size.x) * pixelPerUnit;

            vectorEnd.x = (Mathf.Abs(bounds.min.x - vertices[i + 1].x) / bounds.size.x) * pixelPerUnit;
            vectorEnd.y = (Mathf.Abs(bounds.min.x - vertices[i + 1].y) / bounds.size.x) * pixelPerUnit;

            texture.DrawLine(vectorStart, vectorEnd, Color.red);

            if (i == 0)
                uvPoints[i] = vectorStart / pixelPerUnit;
            uvPoints[i + 1] = vectorEnd / pixelPerUnit;
        }
        texture.Apply();


        return (texture,uvPoints);
    }

    private static void DrawLine(this Texture2D tex, Vector2 p1, Vector2 p2, Color col)
    {
        Vector2 t = p1;
        float frac = 1 / Mathf.Sqrt(Mathf.Pow(p2.x - p1.x, 2) + Mathf.Pow(p2.y - p1.y, 2));
        float ctr = 0;

        while ((int)t.x != (int)p2.x || (int)t.y != (int)p2.y)
        {
            t = Vector2.Lerp(p1, p2, ctr);
            ctr += frac;
            tex.SetPixel((int)t.x, (int)t.y, col);
        }
    }

    public static Vector2[] CreateUVRectInOneTextureWithBorder(Vector3[] vertices, float width, Vector2 ind, float length)
    {
        List<Vector2> uv = new List<Vector2>();
        Vector2 min = new Vector2(ind.x * length, ind.y * length);
        Vector2 max = new Vector2(ind.x * length + length, ind.y * length + length);
        uv.AddRange(CreateUVRect(vertices.Take(vertices.Count() / 2).ToArray(), min, max, length));
        uv.AddRange(CreateUVRect(vertices.Skip(vertices.Count() / 2).ToArray(), min, max, length - (width * length * 2), width * length));
        return uv.ToArray();
    }

    public static Vector2[] CreateUVRectInOneTextureAroundEdges(Vector3[] vertices, float width, Vector2 ind, float length)
    {
        List<Vector2> uv = new List<Vector2>();
        Vector2 min = new Vector2(ind.x * length, ind.y * length);
        Vector2 max = new Vector2(ind.x * length + length, ind.y * length + length);
        uv.AddRange(CreateUVRect(vertices, min, max, length - (width * length * 2), 0.2f * length));
        return uv.ToArray();
    }

    public static Vector2[] CreateUVRectInOneTexture(Vector3[] vertices,  Vector2 ind, float length)
    {
        List<Vector2> uv = new List<Vector2>();
        float k = length / 10f;
        Vector2 min = new Vector2(ind.x * length + k, ind.y * length + k);
        Vector2 max = new Vector2(ind.x * length + length - k, ind.y * length + length - k);
        var bounds = GetBounds(vertices);
        float maxSize = bounds.size.x > bounds.size.y ? bounds.size.x : bounds.size.y;
        
        foreach (var vert in vertices)
        {
            var vector = Vector2.zero;
            vector.x = (vert.x - bounds.min.x) / maxSize * (max.x - min.x) + min.x;
            vector.y = (vert.y - bounds.min.y) / maxSize * (max.y - min.y) + min.y;
            uv.Add(vector);
        }
        return uv.ToArray();
    }
}


