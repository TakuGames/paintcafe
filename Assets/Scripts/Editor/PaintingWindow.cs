﻿using SVGConverter;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TheTide.utils;
using UnityEditor;
using UnityEngine;

public class PaintingWindow : EditorWindow
{
    public static PaintingWindow paintingWindow;
    public PaintingObjectData paintingObjectData;
    public GameObject _prefab;
    // declaring our serializable object, that we are working on
    private SerializedObject serializedObj;

    public  Material MeshMaterial;
    public  Material BorderMaterial;

    private GameObject painting;
    
    public bool _isNewObject;


    [MenuItem("Window/PaintingWindow")]
    public static void Init()
    {
        // initialize window, show it, set the properties
        paintingWindow = GetWindow<PaintingWindow>(false, "PaintingWindow", true);
        paintingWindow.Show();
        //paintingWindow.paintings = new List<GameObject>();
        //paintingWindow.Populate();
    }



    private void OnGUI()
    {

        //EditorGUILayout.Toggle("is new object", _isNewObject);

        
        GUILayout.Label("PaintingWindow", EditorStyles.boldLabel);
        var borderWindow = new SerializedObject(this);


        var ino = borderWindow.FindProperty("_isNewObject");
        EditorGUILayout.PropertyField(ino);
        ino.Reset();

        if (this._isNewObject)
        {
            var ob = borderWindow.FindProperty("paintingObjectData");
            EditorGUILayout.PropertyField(ob);
            ob.Reset();
        }
        else
        {
            EditorGUI.BeginChangeCheck();
            var prefab = borderWindow.FindProperty("_prefab");
            EditorGUILayout.PropertyField(prefab);
            prefab.Reset();
            if (EditorGUI.EndChangeCheck())
            {
                Debug.Log("v");
                
            }
            
        }
        

        for (int i = 0; i < 4; i++)
            EditorGUILayout.Space();

        var meshMat = borderWindow.FindProperty("MeshMaterial");
        EditorGUILayout.PropertyField(meshMat);
        meshMat.Reset();
        var borderMaterial = borderWindow.FindProperty("BorderMaterial");
        EditorGUILayout.PropertyField(borderMaterial);
        borderMaterial.Reset();

        for (int i = 0; i < 4; i++)
            EditorGUILayout.Space();

        borderWindow.ApplyModifiedProperties();



        if(paintingObjectData != null)
        {
            // initialization of the serializedObj, that we are working on
            serializedObj = new SerializedObject(paintingObjectData);

            // Starting our manipulation
            // We're doing this before property rendering           
            serializedObj.Update();
            //// Gets the property of our asset and create a field with its value
            EditorGUILayout.PropertyField(serializedObj.FindProperty("paintingSetting"), new GUIContent("PaintingSetting"), true);

            // Apply changes
            serializedObj.ApplyModifiedProperties();
        }

        ButtonForCreate();


        //if (GUILayout.Button("Save Borders"))
        //{
        //    var select = Selection.activeGameObject;
        //    if (select == null) return;


        //    var meshes = select.transform.GetComponentsInChildren<PaintingFragment>()
        //                        .OrderBy(pf => pf.index)
        //                        .Select(pf => Mesh.Instantiate( pf.border.GetComponent<MeshFilter>().sharedMesh))
        //                        .Select(m => new MeshSubstitute { verts = m.vertices, triangles = m.triangles });
        //    paintingObjectData.Borders = meshes.ToList();
        //    EditorUtility.SetDirty(paintingObjectData);
        //}
        //if (GUILayout.Button("Reset Borders"))
        //{
        //    paintingObjectData.Borders.Clear();
        //    EditorUtility.SetDirty(paintingObjectData);
        //}

        ButtonForSave();

        if (GUILayout.Button("Delete"))
        {
            Delete();
        }
        //if (GUILayout.Button("Reload"))
        //{

        //    MeshBuilder.BorderMaterial = this.BorderMaterial;
        //    MeshBuilder.MeshMaterial = this.MeshMaterial;

        //    //var select = Selection.activeGameObject;
        //    //if(select != null)
        //    //{
        //    //    var pr = select.GetComponent<PaintingReload>();
        //    //    if (pr != null)
        //    //        if(pr.PaintingObjectData != null)
        //    //        {
        //                //pr.GetComponent<PaintingReload>().Reload();
        //                var assetPath = AssetDatabase.GetAssetPath(_prefab);
        //                 var pref = PrefabUtility.LoadPrefabContents(assetPath);
        //                pref.GetComponent<PaintingReload>().Reload();
        //    Save(pref);
        //    //Instantiate(pref);
        //                PrefabUtility.SaveAsPrefabAsset(pref, assetPath);
        //                PrefabUtility.UnloadPrefabContents(pref);
        //    //        }
                        
        //    //}
                
        //}
        


        borderWindow.ApplyModifiedProperties();
    }

    private void ButtonForSave()
    {
        if (_isNewObject)
        {
            if (GUILayout.Button("Save to Prefab"))
            {
                var select = Selection.activeGameObject;
                if (select == null) return;

                var pr = select.GetComponent<PaintingReload>();
                if (pr == null) return;

                var path = EditorUtility.SaveFilePanel("title", "Assets/Prefabs", "paint-" + pr.name, "prefab");

                if (path.Length == 0) return;

                Debug.Log(path);

                Save(select);

                PrefabUtility.SaveAsPrefabAsset(select, path);
            }
        }
        else
        {

        }
    }

    private void ButtonForCreate()
    {
        if (_isNewObject)
        {
            if (GUILayout.Button("Create"))
            {


                MeshBuilder.BorderMaterial = this.BorderMaterial;
                MeshBuilder.MeshMaterial = this.MeshMaterial;
                SVGToVectors.BezierSegments = paintingObjectData.paintingSetting.BezierSegments;
                SVGToVectors.ElipseSegments = paintingObjectData.paintingSetting.ElipseSegments;
                var shapes = SVGToVectors.Svg(paintingObjectData.painting.File);
                var ColorElements = MeshBuilder.GenerateMesh(shapes.vectors, shapes.colors, paintingObjectData.paintingSetting.BorderWidth, paintingObjectData.paintingSetting.LittleFragmentArea, null, paintingObjectData.Borders);
                GameObject go = new GameObject(paintingObjectData.name);
                var pr = go.AddComponent<PaintingReload>();
                pr.PaintingObjectData = paintingObjectData;
                foreach (var item in ColorElements.Values)
                {
                    foreach (var gm in item)
                    {
                        gm.border.AddComponent<SerializeMesh>();
                        gm.gameObject.AddComponent<SerializeMesh>();
                        gm.transform.SetParent(go.transform);
                    }
                }
                Delete();
                Save(go);
                painting = go;
            }
        }
        else
        {
            if (GUILayout.Button("Show"))
            {
                var go = Instantiate(_prefab);
                Delete();
                painting = go;
            }
        }
    }

    private void Create()
    {

    }

    private void Save(GameObject go)
    {
        var SMs = go.GetComponentsInChildren<SerializeMesh>();
        foreach (var SM in SMs)
        {
            SM.Serialize();
        }
    }

    private void Delete()
    {
        if (painting != null)
        {
            DestroyImmediate(painting);
        }
    }
}
