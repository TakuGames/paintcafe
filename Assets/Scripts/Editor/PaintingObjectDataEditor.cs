﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(PaintingObjectData))]
public class PaintingObjectDataEditor : Editor
{
    public override void OnInspectorGUI()
    {
        var paintOD = target as PaintingObjectData;

        EditorGUILayout.Toggle("isPainted",paintOD.isPainted);

        


        var serializedObject = new SerializedObject(paintOD);

        


        serializedObject.Update();

        var paintFile = serializedObject.FindProperty("painting");
        EditorGUILayout.PropertyField(paintFile);
        paintFile.Reset();
        serializedObject.ApplyModifiedProperties();

        //EditorGUIUtility.LookLikeInspector();
        SerializedProperty tps = serializedObject.FindProperty("Borders");
        EditorGUI.BeginChangeCheck();
        EditorGUILayout.PropertyField(tps, true);
        if (EditorGUI.EndChangeCheck())
            serializedObject.ApplyModifiedProperties();
        //EditorGUIUtility.LookLikeControls();
    }
}
