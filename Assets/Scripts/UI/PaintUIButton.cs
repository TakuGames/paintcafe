﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PaintUIButton : MonoBehaviour
{
    private Color _color;
    private int _count;
    private Image _image;
    private Image _chieldImage;
    private Button _button;
    
    [SerializeField] private TMPro.TextMeshProUGUI _number;


    public int ColorInd
    {
        set
        {
            _number.text = value.ToString();
        }
    }

    public void SetSize(float size)
    {
        transform.localScale = new Vector3(size, size, 1);
    }

    public Color Color {
        get => _color;
        set
        {
            _color = value;
            var color = value;
            _chieldImage.color = _color;
            color.a = 0.5f;
            _image.color = color;
            
        }
    }

    public int Count { get => _count; set => _count = value; }

    private void Awake()
    {
        var chield = transform.GetChild(0);
        _image = GetComponent<Image>();
        _chieldImage = chield.GetComponent<Image>(); 
        _button = GetComponent<Button>();
        _button.onClick.AddListener(SetColor);

        _image.material.SetTexture("_MainTex", _image.mainTexture);
    }

    private void SetColor()
    {
        GameManager.instance.GetSingletonComponent<PaintsUI>().SetCurColor(Color);
    }

    private void OnDestroy()
    {
        _button.onClick.RemoveListener(SetColor);
    }

    public void PaintReduction(int curCount)
    {
        _chieldImage.fillAmount = (curCount *1f) / Count;
    }
}
