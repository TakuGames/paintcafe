﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaintAllButton : MonoBehaviour
{
    public void Paint()
    {
        StartCoroutine( GameManager.instance.GetSingletonComponent<PaintingController>().PaintAll());
    }
}
