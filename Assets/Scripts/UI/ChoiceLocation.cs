﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Rendering.PostProcessing;

[Serializable]
public class BackFromPaint : UnityEvent { }

[Serializable]
public class SelectPaint : UnityEvent { }

public class ChoiceLocation : MonoBehaviour
{


    [SerializeField] private GameObject MainPanel;
    [SerializeField] private GameObject curLoc;
    [SerializeField] private GameObject _mainScreen;

    public BackFromPaint BackFromPaint;
    public SelectPaint SelectPaint;

    private Camera _camera;

    private void Start()
    {
        _camera = Camera.main;
        var ls = LevelSingleton.Instance; //LevelSetings.Instance;



        if (ls.CurLocation == null) return;

        ls.CurLocation.GetComponent<PaintedManager>().Write();
        SetLocation(ls.CurLocation);
        _mainScreen.SetActive(false);
        GameManager.instance.GetSingletonComponent<Money>().ClosePaint();
        BackFromPaint.Invoke();
    }

    public void SetLocation(GameObject location)
    {
        LevelSingleton.Instance.CurLocation = location;
        
        var go = Instantiate(location, Vector3.zero, Quaternion.identity);
        _camera.transform.position = go.transform.position.ToVector2().ToVector3(_camera.transform.position.z);

        var size = go.GetComponent<SpriteRenderer>().bounds.size.y / 2;
        if (size > go.GetComponent<SpriteRenderer>().bounds.size.x / 2 / _camera.aspect)
            _camera.orthographicSize = go.GetComponent<SpriteRenderer>().bounds.size.x / 2 / _camera.aspect;
        else
            _camera.orthographicSize = size;


        Debug.Log(_camera.aspect);
        curLoc = go;
        MainPanel.SetActive(false);
    }

    public void SetCurLocation(GameObject location)
    {
        SetLocation(location);
    }

    public void Back()
    {
        MainPanel.SetActive(true);
        Destroy(curLoc);
        LevelSingleton.Instance.CurLocation = null;
    }


}
