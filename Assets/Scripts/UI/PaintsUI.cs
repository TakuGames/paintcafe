﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class PaintsUI : MonoBehaviour
{
    [SerializeField] private GameObject _paintPref;
    
    private Dictionary<Color, PaintUIButton> _paintButtons;
    private PaintUIButton curButton;

    void Awake()
    {
        _paintButtons = new Dictionary<Color, PaintUIButton>();
        GameManager.instance.GetSingletonComponent<PaintingController>().SetColors += SetColors;
        GameManager.instance.GetSingletonComponent<PaintingController>().ChangeColor += ChangeColor;    
    }

    private void OnEnable()
    {

        GameManager.instance.SetSingletonComponent<PaintsUI>(this);
    }

    

    public void SetColors(Color[] colors, int[] counts)
    {
        for (int i = 0; i < colors.Length; i++)
        {
            var button = Instantiate(_paintPref, transform);
            var pb = button.GetComponent<PaintUIButton>();

            pb.Count = counts[i];
            pb.Color = colors[i];
            pb.ColorInd = i+1;
            _paintButtons.Add(colors[i], pb);
        }

        StartCoroutine(AfterStart());       
    }

    private IEnumerator AfterStart()
    {
        yield return new WaitForEndOfFrame();

        Vector3[] vec = new Vector3[4];
        GetComponent<RectTransform>().GetWorldCorners(vec);
        float dist = Mathf.Abs(vec[0].x - vec[3].x) / 2f;  //GetComponent<RectTransform>().sizeDelta.x ;
        var cur = transform.position;
        cur.x += dist;
        transform.position = cur;
    }

    public void ChangeColor(Color colors, int curCount)
    {
        _paintButtons[colors].PaintReduction(curCount);
        if (curCount <= 0)
            Destroy(_paintButtons[colors].gameObject);
    }

    public void SetCurColor(Color color)
    {
        if (curButton != null)
            curButton.SetSize(1);
        curButton = _paintButtons[color];
        curButton.SetSize(1.4f);
        GameManager.instance.GetSingletonComponent<PaintingController>().CurrentColor = color;
    }

    private void OnDisable()
    {
        //GameManager.instance.GetSingletonComponent<PaintingController>().SetColors -= SetColors;
        //GameManager.instance.GetSingletonComponent<PaintingController>().ChangeColor -= ChangeColor;

        GameManager.instance.RemoveSingletonComponent<PaintsUI>(this);
    }
    
}
