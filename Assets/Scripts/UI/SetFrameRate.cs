﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;


public class SetFrameRate : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI text;

    private void Awake()
    {
        Application.targetFrameRate = 60;
    }

    private void Update()
    {
        text.text = "" + (1 / Time.deltaTime);
    }
}
