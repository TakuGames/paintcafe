﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FragmentSearching : MonoBehaviour
{
    [SerializeField] private PinchZoom zoom;

    public void Search()
    {
        var pc = GameManager.instance.GetSingletonComponent<PaintingController>().GetFirstFragmentfromCurColor();

        if (pc == null) return;

        GameManager.instance.GetSingletonComponent<PaintingController>().SelectFragmentWithLight(pc.index);

        zoom.CameraToPoint(pc.bounds.center, pc.bounds.size.x);
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
            if (!Click.StopGame)
                GameManager.instance.GetSingletonComponent<PaintingController>().ClearFragmentWithLight();
    }
}
