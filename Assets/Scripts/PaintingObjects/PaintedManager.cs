﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaintedManager : MonoBehaviour
{
    public string LocationName;
    public PaintingObjectData[] painteds;


    private void Awake()
    {
        Read();
    }

    public void Write()
    {
        string s = "";
        //bool[] mas = new bool[painteds.Length];
        for (int i = 0; i < painteds.Length; i++)
        {
            s += painteds[i].isPainted ? "1" : "0";
        }
        PlayerPrefs.SetString(LocationName, s);
    }

    private void Read()
    {
        if (!PlayerPrefs.HasKey(LocationName)) return;

        string s = PlayerPrefs.GetString(LocationName);
        for (int i = 0; i < s.Length; i++)
        {
            painteds[i].isPainted = s[i] == '1' ? true : false;
        }
    }

    private void Clear()
    {
        string s = "";
        for (int i = 0; i < painteds.Length; i++)
        {
            painteds[i].isPainted = false;
            s += "0";
        }
        PlayerPrefs.SetString(LocationName, s);
    }

    private void OnDestroy()
    {
        Clear();
        Write();
    }
}
