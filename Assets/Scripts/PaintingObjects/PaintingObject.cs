﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Rendering.PostProcessing;
using UnityEngine.EventSystems;

public class PaintingObject : MonoBehaviour
{
    [SerializeField] private PaintingObjectData _data;


    //private Energy _energy;

    private SpriteRenderer _sr;
    private bool click;
    private GameObject Rainbow;
    private bool rain;

    //public Energy Energy { get => _energy; set => _energy = value; }

    private void Awake()
    {
        Rainbow = Instantiate( Resources.Load("Rainbow") as GameObject);
        Rainbow.transform.localPosition = Vector3.zero;
        Rainbow.SetActive(false);
        rain = false;
    }

    private void Start()
    {
        click = false;
        _sr = GetComponent<SpriteRenderer>();
        GetComponent<BoxCollider2D>().size = _sr.bounds.size;

        var sr = GetComponent<SpriteRenderer>();
        MaterialPropertyBlock materialPropertyBlock = new MaterialPropertyBlock();
        sr.GetPropertyBlock(materialPropertyBlock);
        materialPropertyBlock.SetFloat("_GrayValue", _data.isPainted ? 1 : 0);
        sr.SetPropertyBlock(materialPropertyBlock);

        if (_data.coordinate != null)
            Decrease();
    }

    private void OnMouseDown()
    {
        StartCoroutine(Increase());
    }

    private void Update()
    {
        if (rain)
            Rainbow.transform.position = transform.position;
    }

    private IEnumerator Increase()
    {
        if (!click && !_data.isPainted)
        {
            EventSystem.current.gameObject.SetActive(false);

            click = true;

            var camera = Camera.main;

            var width = (camera.orthographicSize  * camera.aspect) / _sr.bounds.size.x  ;
            Vector3 scale = new Vector3(width, width, 1);
            var pos = camera.transform.position;
            pos.z = transform.position.z ;
            _sr.sortingOrder = 22;
            //StartCoroutine(Scaling(transform.position, transform.localScale,pos , scale));
            Debug.Log("Increase");
            _data.coordinate = camera.transform.position;


            var ls = LevelSingleton.Instance;
            ls.CurPainting = _data;
            var s1 = SceneManager.LoadSceneAsync(1);
            s1.allowSceneActivation = false;

            transform.parent.GetComponent<Animator>().SetTrigger("Dark");
            Rainbow.SetActive(true);
            rain = true;
            yield return StartCoroutine(Scaling(transform.position, transform.localScale, pos, scale, true));
            //camera.gameObject.GetComponent<StartRainbow>().StartRainBow();
            
            

            

            yield return new WaitForSeconds(.5f);

            s1.allowSceneActivation = true;
        }
    }

    private void Decrease()
    {
        if (click) return;

        var camera = Camera.main;
        var width = (camera.orthographicSize * camera.aspect) / _sr.bounds.size.x;
        Vector3 scale = new Vector3(width, width, 1);

        StartCoroutine(Scaling((Vector3)_data.coordinate, scale , transform.position, transform.localScale));

        _data.coordinate = null;
    }

    private IEnumerator Scaling(Vector3 startPosition, Vector3 startScale,Vector3 endPosition, Vector3 endScale, bool aberation = false)
    {
        var wait = new WaitForEndOfFrame();
        var curTime = 0f;
        var ls = LevelSingleton.Instance;
        ChromaticAberration aberration = null;
        if (ls.postProcessVolume != null)
            ls.postProcessVolume.profile.TryGetSettings(out aberration);
        Debug.Log("Scaling");
        while (curTime < 1)
        {
            transform.position = Vector3.Lerp(startPosition, endPosition, curTime);
            transform.localScale = Vector3.Lerp(startScale, endScale, curTime);

            if (aberration != null && aberation)
                aberration.intensity.value = curTime;
            
            curTime += Time.deltaTime ;
            yield return wait;
        }
        transform.position = Vector3.Lerp(startPosition, endPosition, 1);
        transform.localScale = Vector3.Lerp(startScale, endScale, 1);
    }
}
