﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;




[CreateAssetMenu(fileName = " new Painting Object Data", menuName = " Painting Object Data")]
public class PaintingObjectData : ScriptableObject
{
    public SVGPainting painting;

    public bool isPainted;


    public Vector3? coordinate;

    public PaintingSetting paintingSetting = new PaintingSetting {BezierSegments = 2, ElipseSegments = 14, BorderWidth = 1, LittleFragmentArea = 10 };

    public List<MeshSubstitute> Borders = new List<MeshSubstitute>();

    public List<NumberConfigs> Numbers = new List<NumberConfigs>();
    
}

[Serializable]
public struct MeshSubstitute
{
    public Vector3[] verts;
    public int[] triangles; 
}

[Serializable]
public struct NumberConfigs
{
    public Vector3 position;
    public Vector2 size;
}
