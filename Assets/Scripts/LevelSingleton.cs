﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class LevelSingleton 
{
    private static LevelSingleton _instance;

    public PaintingObjectData CurPainting;
    public bool Menu;
    public GameObject CurLocation;

    public PostProcessVolume postProcessVolume;

    public static LevelSingleton Instance
    {
        get
        {
            if (_instance == null)
                _instance = new LevelSingleton();
            return _instance;
        }
    }


}
