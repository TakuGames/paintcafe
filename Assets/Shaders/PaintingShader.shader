﻿Shader "Custom/PaintingShader"
{
    Properties
    {
        _Color ("Color", Color) = (1,1,1,1)
        _CheckTex ("Albedo (RGB)", 2D) = "white" {}
		_MainTex("Albedo (RGB)", 2D) = "white" {}
		
	}
		SubShader
	{
		Tags
			{
				"Queue" = "Transparent"
				"IgnoreProjector" = "True"
				"RenderType" = "Transparent"
				"PreviewType" = "Plane"
				"CanUseSpriteAtlas" = "True"
			}
		Cull Off
			Lighting Off
		//Blend One OneMinusSrcAlpha
Pass{

		CGPROGRAM
		// Upgrade NOTE: excluded shader from DX11, OpenGL ES 2.0 because it uses unsized arrays
		#pragma exclude_renderers d3d11 gles
						#pragma vertex vert
						#pragma fragment frag
						#pragma multi_compile _ PIXELSNAP_ON
						#pragma shader_feature ETC1_EXTERNAL_ALPHA
						#include "UnityCG.cginc"


				sampler2D _CheckTex;
				sampler2D _MainTex;
				float4 _Color;


						struct appdata_t
						{
							float4 vertex   : POSITION;
							float4 color    : COLOR;
							float2 texcoord : TEXCOORD0;
						};

						struct v2f
						{
							float4 vertex   : SV_POSITION;
							fixed4 color : COLOR;
							float2 texcoord  : TEXCOORD0;
						};

						v2f vert(appdata_t IN)
						{
							v2f OUT;
							OUT.vertex = UnityObjectToClipPos(IN.vertex);
							OUT.texcoord = IN.texcoord;
							OUT.color = IN.color;
							#ifdef PIXELSNAP_ON
												OUT.vertex = UnityPixelSnap(OUT.vertex);
							#endif

							return OUT;
						}

						fixed4 frag(v2f IN) : SV_Target
						{
						if (IN.texcoord.y < (0.5 - _FillRadius) || IN.texcoord.y >(0.5 + _FillRadius) || IN.texcoord.x < (0.5 - _FillRadius) || IN.texcoord.x >(0.5 + _FillRadius))
								return _CoveringColor;

							return _Color;
						}
							ENDCG
			}
			
	}
		FallBack "Diffuse"
}
