﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    public class UvGeneratorTests
    {
        // A Test behaves as an ordinary method
        [Test]
        public void GetVertAnglePointsIndTestsSimplePasses()
        {
            var vert = new Vector3[] { new Vector2(-1, 1), new Vector2(0, 1), new Vector2(1, 1), new Vector2(1, 0), new Vector2(1, -1), new Vector2(0, -1), new Vector2(-1, -1), new Vector2(-1, 0), };
            var pointAngles = UvBorderGenerator.GetVertAnglePointsInd(vert);
            Assert.AreEqual((0, 2, 4, 6), pointAngles);
        }
        public class CreteRectTests
        {

            [Test]
            public void UP()
            {
                var vert = new Vector3[] { new Vector2(-1, 1), new Vector2(0, 1), new Vector2(1, 1), new Vector2(1, 0), new Vector2(1, -1), new Vector2(0, -1), new Vector2(-1, -1), new Vector2(-1, 0), };
                var expected = new Vector2[] { new Vector2(-1, 1), new Vector2(0, 1) };
                var actual = UvBorderGenerator.CreateRect(vert);
                Assert.AreEqual(expected, actual.up);
            }

            [Test]
            public void Down()
            {
                var vert = new Vector3[] { new Vector2(-1, 1), new Vector2(0, 1), new Vector2(1, 1), new Vector2(1, 0), new Vector2(1, -1), new Vector2(0, -1), new Vector2(-1, -1), new Vector2(-1, 0), };
                var expected = new Vector2[] { new Vector2(1, -1), new Vector2(0, -1) };
                var actual = UvBorderGenerator.CreateRect(vert);
                Assert.AreEqual(expected, actual.down);
            }

            [Test]
            public void Right()
            {
                var vert = new Vector3[] { new Vector2(-1, 1), new Vector2(0, 1), new Vector2(1, 1), new Vector2(1, 0), new Vector2(1, -1), new Vector2(0, -1), new Vector2(-1, -1), new Vector2(-1, 0), };
                var expected = new Vector2[] { new Vector2(1, 1), new Vector2(1, 0) };
                var actual = UvBorderGenerator.CreateRect(vert);
                Assert.AreEqual(expected, actual.right);
            }

            [Test]
            public void Left()
            {
                var vert = new Vector3[] { new Vector2(-1, 1), new Vector2(0, 1), new Vector2(1, 1), new Vector2(1, 0), new Vector2(1, -1), new Vector2(0, -1), new Vector2(-1, -1), new Vector2(-1, 0), };
                var expected = new Vector2[] { new Vector2(-1, -1), new Vector2(-1, 0) };
                var actual = UvBorderGenerator.CreateRect(vert);
                Assert.AreEqual(expected, actual.left);
            }
        }


        public class CreteUVRectTests
        {

            [Test]
            public void UP()
            {
                var vert = new Vector3[] { new Vector2(-1, 1), new Vector2(0, 1), new Vector2(1, 1), new Vector2(1, 0), new Vector2(1, -1), new Vector2(0, -1), new Vector2(-1, -1), new Vector2(-1, 0), };
                var expected = new Vector2[] { new Vector2(0, 1), new Vector2(0.5f, 1) };
                var array = UvBorderGenerator.CreateUVRect(vert);
                var actual = new Vector2[] { array[0], array[1] };
                Assert.AreEqual(expected, actual);
            }

            [Test]
            public void Down()
            {
                var vert = new Vector3[] { new Vector2(-1, 1), new Vector2(0, 1), new Vector2(1, 1), new Vector2(1, 0), new Vector2(1, -1), new Vector2(0, -1), new Vector2(-1, -1), new Vector2(-1, 0), };
                var expected = new Vector2[] { new Vector2(1, 0), new Vector2(0.5f, 0) };
                var array = UvBorderGenerator.CreateUVRect(vert);
                var actual = new Vector2[] { array[4], array[5] };
                Assert.AreEqual(expected, actual);
            }

            [Test]
            public void Right()
            {
                var vert = new Vector3[] { new Vector2(-1, 1), new Vector2(0, 1), new Vector2(1, 1), new Vector2(1, 0), new Vector2(1, -1), new Vector2(0, -1), new Vector2(-1, -1), new Vector2(-1, 0), };
                var expected = new Vector2[] { new Vector2(1, 1), new Vector2(1, 0.5f) };
                var array = UvBorderGenerator.CreateUVRect(vert);
                var actual = new Vector2[] { array[2], array[3] };
                Assert.AreEqual(expected, actual);
            }

            [Test]
            public void Left()
            {
                var vert = new Vector3[] { new Vector2(-1, 1), new Vector2(0, 1), new Vector2(1, 1), new Vector2(1, 0), new Vector2(1, -1), new Vector2(0, -1), new Vector2(-1, -1), new Vector2(-1, 0), };
                var expected = new Vector2[] { new Vector2(0, 0), new Vector2(0, 0.5f) };
                var array = UvBorderGenerator.CreateUVRect(vert);
                var actual = new Vector2[] { array[6], array[7] };
                Assert.AreEqual(expected, actual);
            }

        }



            [Test]
            public void CreteUVRectTestsWithTexture()
            {
                var vert = new Vector3[] { new Vector2(-1, 1), new Vector2(0, 1), new Vector2(1, 1), new Vector2(1, 0), new Vector2(1, -1), new Vector2(0, -1), new Vector2(-1, -1), new Vector2(-1, 0), };
                var expected = new Vector2[] { new Vector2(0, 1), new Vector2(0.5f, 1), new Vector2(1, 1), new Vector2(1, 0.5f), new Vector2(1, 0), new Vector2(0.5f, 0) , new Vector2(0, 0), new Vector2(0, 0.5f) };
                var array = UvBorderGenerator.CreateTextureAndUvRect(vert,100).uv;
            var actual = array;
                Assert.AreEqual(expected, actual);
            }

        [Test]
        public void CreateUVRectInOneTextureTest()
        {
            var vert = new Vector3[] {  new Vector2(-1, 1),  new Vector2(1, 1),  new Vector2(1, -1),  new Vector2(-1, -1), new Vector2(-0.9f, 0.9f), new Vector2(0.9f, 0.9f), new Vector2(0.9f, -0.9f), new Vector2(-0.9f, -0.9f), };
            //var expected = new Vector2[] {  new Vector2(0,1) , new Vector2(0.5f,1) ,  new Vector2(0.5f,0.5f) ,  new Vector2(0,0.5f), new Vector2(0.05f, 0.95f), new Vector2(0.95f, 0.95f), new Vector2(0.95f, 0.45f), new Vector2(0.05f, 0.45f), };
            var expected = new Vector2[] { new Vector2(0, 0.5f), new Vector2(0.5f, 0.5f), new Vector2(0.5f, 0), new Vector2(0, 0), new Vector2(0.05f, 0.45f), new Vector2(0.45f, 0.45f), new Vector2(0.45f, 0.05f), new Vector2(0.05f, 0.05f), };
            var array = UvBorderGenerator.CreateUVRectInOneTextureWithBorder(vert, 0.1f, Vector2.zero, 1f / 2f);
            var actual = array;
            //Assert.AreEqual(expected, actual);
            Assert.IsTrue(AreEquelVector2Array(expected, actual));
        }

        [Test]
        public void CreateUVRectInOneTextureTest2()
        {
            var vert = new Vector3[] {     new Vector2(-1, 1),   new Vector2(0,1),         new Vector2(1, 1),       new Vector2(1, 0),        new Vector2(1, -1),   new Vector2(0, -1),    new Vector2(-1, -1), new Vector2(-1, 0),    new Vector2(-0.9f, 0.9f),  new Vector2(0 , 0.9f),     new Vector2(0.9f, 0.9f),   new Vector2(0.9f, 0),      new Vector2(0.9f, -0.9f),  new Vector2(0, -0.9f),     new Vector2(-0.9f, -0.9f), new Vector2(-0.9f, 0), };
            var expected = new Vector2[] { new Vector2(0, 0.5f), new Vector2(0.25f, 0.5f), new Vector2(0.5f, 0.5f), new Vector2(0.5f, 0.25f), new Vector2(0.5f, 0), new Vector2(0.25f, 0), new Vector2(0, 0),   new Vector2(0, 0.25f), new Vector2(0.05f, 0.45f), new Vector2(0.25f, 0.45f), new Vector2(0.45f, 0.45f), new Vector2(0.45f, 0.25f), new Vector2(0.45f, 0.05f), new Vector2(0.25f, 0.05f), new Vector2(0.05f, 0.05f), new Vector2(0.05f, 0.25f), };
            var array = UvBorderGenerator.CreateUVRectInOneTextureWithBorder(vert, 0.1f, Vector2.zero, 1f / 2f);
            var actual = array;
            //Assert.AreEqual(expected, actual);
            Assert.IsTrue(AreEquelVector2Array(expected, actual));
        }


        [Test]
        public void CreateUVRectInOneTextureTest3()
        {
            var vert = new Vector3[] { new Vector2(-1, 1), new Vector2(0, 1), new Vector2(1, 1), new Vector2(1, 0), new Vector2(1, -1), new Vector2(0, -1), new Vector2(-1, -1), new Vector2(-1, 0), new Vector2(-0.9f, 0.9f), new Vector2(0, 0.9f), new Vector2(0.9f, 0.9f), new Vector2(0.9f, 0), new Vector2(0.9f, -0.9f), new Vector2(0, -0.9f), new Vector2(-0.9f, -0.9f), new Vector2(-0.9f, 0), };
            var expected = new Vector2[] { new Vector2(0, 1), new Vector2(0.25f, 1), new Vector2(0.5f, 1), new Vector2(0.5f, 0.75f), new Vector2(0.5f, 0.5f), new Vector2(0.25f, 0.5f), new Vector2(0, 0.5f), new Vector2(0, 0.75f), new Vector2(0.05f, 0.95f), new Vector2(0.25f, 0.95f), new Vector2(0.45f, 0.95f), new Vector2(0.45f, 0.75f), new Vector2(0.45f, 0.55f), new Vector2(0.25f, 0.55f), new Vector2(0.05f, 0.55f), new Vector2(0.05f, 0.75f), };
            var array = UvBorderGenerator.CreateUVRectInOneTextureWithBorder(vert, 0.1f, new Vector2(0,1), 1f / 2f);
            var actual = array;
            //Assert.AreEqual(expected, actual);
            Assert.IsTrue(AreEquelVector2Array(expected, actual));
        }

   

        private bool AreEquelVector2Array(Vector2[] a1, Vector2[] a2)
        {
            bool result = true;
            var exp = 0.000001f;
            for (int i = 0; i < a1.Length; i++)
            {
                if (a1[i].x - a2[i].x > exp)
                    result = false;
                if (a1[i].y - a2[i].y > exp)
                    result = false;
            }
            return result;
        }
    }
}
